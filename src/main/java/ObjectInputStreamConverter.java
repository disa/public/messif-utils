/*
 * ObjectInputStreamConverter
 */

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.ObjectStreamClass;
import java.io.StreamCorruptedException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import messif.network.NetworkNode;

/**
 * Special variant of ObjectInputStream that allow loading objects with different serial version UUIDs.
 * There must be a conversion method for every serial version that can be loaded and is
 * incompatible with the actual version:
 * <pre>
 * private void readObjectVerXXXXXX(java.io.ObjectInputStream stream) throws IOException, ClassNotFoundException;
 * </pre>
 * Where the XXXXX must match the incompatible serial version UUID.
 * Note that instead of negative numbers an 'N' character is applied, so for example the
 * method that loads data for serial version -123 should look like:
 * <pre>
 * private void readObjectVerN123(java.io.ObjectInputStream stream) throws IOException, ClassNotFoundException {
 *    ...
 * }
 * </pre>
 * <p>
 * <b>Warning:</b> this class is a hack that exploits the internals of the {@link ObjectStreamClass} and
 * it can cease to function at any time. Note also that the JVM where this class is used <b>should never</b>
 * be used for anything else than serializing the deserialized object.
 * </p>
 * @author xbatko
 */
public class ObjectInputStreamConverter extends ObjectInputStream {

    /** Field of ObjectStreamClass where the serial version UUID is stored */
    private final Field forceSetSerialVersion;
    /** Field of ObjectStreamClass where the read object method invoker is stored */
    private final Field forceSetReadObjectMethod;

    /**
     * Creates an ObjectInputStream that reads from the specified InputStream.
     * The instance will allow loading objects with different serial version UUIDs,
     * if there is a conversion method present in the class.
     *
     * @param in input stream to read from
     * @throws StreamCorruptedException if the stream header is incorrect
     * @throws IOException if an I/O error occurs while reading stream header
     * @throws NullPointerException if <code>in</code> is <code>null</code>
     */
    public ObjectInputStreamConverter(InputStream in) throws StreamCorruptedException, NullPointerException, IOException {
        super(in);

        try {
            // Load field where the serial version UUID is stored
            forceSetSerialVersion = ObjectStreamClass.class.getDeclaredField("suid");
            forceSetSerialVersion.setAccessible(true);

            // Load the field where the read object method invoker is stored
            forceSetReadObjectMethod = ObjectStreamClass.class.getDeclaredField("readObjectMethod");
            forceSetReadObjectMethod.setAccessible(true);
        } catch (NoSuchFieldException e) {
            throw new InternalError("ObjectStreamClass attribute names have changed: " + e.toString());
        }
    }

    /**
     * Returns a method from <code>clazz</code> that loads the previous version of the object.
     * The method should have the following prototype:
     * <pre>
     * private void readObjectVerXXXXXX(java.io.ObjectInputStream stream) throws IOException, ClassNotFoundException;
     * </pre>
     * Where the XXXXX must match the previous serial version UUID.
     * Note that instead of negative numbers an 'N' character is applied, so for example the
     * method that loads data for serial version -123 should look like:
     * <pre>
     * private void readObjectVerN123(java.io.ObjectInputStream stream) throws IOException, ClassNotFoundException {
     *    ...
     * }
     * </pre>
     * 
     * @param clazz
     * @param serialVersion
     * @return
     */
    private Method getCompatibilityMethod(Class<?> clazz, long serialVersion) throws NoSuchMethodException {
        StringBuilder name = new StringBuilder("readObjectVer");
        if (serialVersion < 0)
            name.append('N').append(-serialVersion);
        else
            name.append(serialVersion);

        Method ret = clazz.getDeclaredMethod(name.toString(), ObjectInputStream.class);
        ret.setAccessible(true);
        return ret;
    }

    @Override
    protected ObjectStreamClass readClassDescriptor() throws IOException, ClassNotFoundException {
        // Get the descriptor from stream
        ObjectStreamClass classDesc = super.readClassDescriptor();

        // Get the descriptor for current machine
        Class<?> clazz = Class.forName(classDesc.getName());
        ObjectStreamClass localClassDesc = ObjectStreamClass.lookup(clazz);

        // If the versions match, do nothing
        if (localClassDesc == null || classDesc.getSerialVersionUID() == localClassDesc.getSerialVersionUID())
            return classDesc;

        try {
            forceSetReadObjectMethod.set(localClassDesc, getCompatibilityMethod(clazz, classDesc.getSerialVersionUID()));
            forceSetSerialVersion.set(classDesc, localClassDesc.getSerialVersionUID());
        } catch (NoSuchMethodException e) {
            throw new IOException("There is no compatibility read method for '" + clazz + "' that loads serial version " + classDesc.getSerialVersionUID());
        } catch (IllegalAccessException e) {
            // This should never happen, since all the fields are set "accessible"
            throw new InternalError(e.toString());
        }
        
        return classDesc;
    }

    /**
     * Deserializes one object from the <code>inFile</code> and writes it into the <code>outFile</code>.
     * @param inFile the file to read the object from
     * @param outFile the file to write the object to
     * @throws IOException if there was an I/O error reading or writing the file
     * @throws ClassNotFoundException if the deserialization failed
     */
    public static void convert(File inFile, File outFile) throws IOException, ClassNotFoundException {
        System.out.println("Reading from " + inFile);
        ObjectInputStreamConverter in = new ObjectInputStreamConverter(new FileInputStream(inFile));
        ObjectOutputStream out = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(outFile)));
        Object obj = in.readObject();
        System.out.println("Writing to " + outFile);
        out.writeObject(obj);
        out.close();
        in.close();

        // Try to finalize the object
        try {
            Method method = obj.getClass().getMethod("finalize");
            System.out.println("Calling public finalize method");
            method.invoke(obj);
        } catch (NoSuchMethodException e) {
            // Ignored (no public finalize method)
        } catch (Throwable e) {
            System.out.println("Error calling public finalize method (ignored): " + e);
        }
    }

    /**
     * Uses the converting object stream to deserialize and serialize objects from files.
     * @param args the command line arguments, the two arguments from intput and output files (or dirs) are expected
     * @throws IOException if there was an I/O error reading or writing the file
     * @throws ClassNotFoundException if the deserialization failed
     */
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        if (args.length < 2) {
            System.err.println("Usage: ObjectInputStreamConverter <in file/dir> <out file/dir> [<netnode remap file>]");
            return;
        }

        // Read network node remap file
        if (args.length > 2) {
            NetworkNode.loadHostMappingTable(args[2]);
        }

        // Read in/out files or directories
        File in = new File(args[0]);
        File out = new File(args[1]);
        if (in.isDirectory() || out.isDirectory() || in.equals(out)) {
            if (!in.isDirectory() || !out.isDirectory()) {
                System.err.println("Parameters must be either both files or both directories");
                return;
            }

            // Directories, convert files in the first and write them into second
            for (File file : in.listFiles()) {
                if (file.isFile() && file.canRead())
                    convert(file, new File(out, file.getName()));
            }
        } else {
            convert(in, out);
        }
    }
}
