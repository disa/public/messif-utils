
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import messif.objects.LocalAbstractObject;
import messif.objects.util.ObjectProvidersIterator;
import messif.objects.util.StreamGenericAbstractObjectIterator;
import messif.utility.Convert;

/**
 *
 * @author David Novak, david.novak@fi.muni.cz, Faculty of Informatics, Masaryk University, Brno
 */
public class DivideByUsers {

    private static void printUsage() {
        System.err.println("Usage: DivideByUsers <objects' class> -in <input file> [-out <output directory>] -users <file with a map <object_id user_id>>");
        System.err.println("            File with the map can be read from STDIN -- use '-' (minus) as the file name.");
        System.err.println("            As <input file> you can specify a directory name, so all files in the directory will be processed.");
        System.err.println("            As <output directory> you can specify also '.' (the current directory).");
        System.err.println("            If <output_directory> is omitted, nothing is written to the disk. A test on correctness of such division is performed.");
    }

    /**
     * This class reclusters data read from input file or from all files in the input directory.
     * The reclustering is based on the mapping photoid->userid. 
     * 
     * @param args command line arguments - call without parameters - it displays usage.
     * @throws java.lang.Exception 
     */
    public static void main(String... args) throws Exception {
        try {
            // Parse arguments and open input and output files
            int argIndex = 1;

            // Objects class
            Class<LocalAbstractObject> objectsClass = Convert.getClassForName(args[0], LocalAbstractObject.class);

            // Object input file(s)
            if (!args[argIndex].equals(("-in"))) {
                printUsage();
                return;
            }

            List<StreamGenericAbstractObjectIterator<LocalAbstractObject>> inputStreams =
                    new ArrayList<StreamGenericAbstractObjectIterator<LocalAbstractObject>>();
            File inputFile = new File(args[argIndex + 1]);
            if (inputFile.isFile()) {
                // One file
                inputStreams.add(new StreamGenericAbstractObjectIterator<LocalAbstractObject>(objectsClass, args[argIndex + 1]));
            } else {
                // Use all files in the directory
                for (File f : inputFile.listFiles()) {
                    inputStreams.add(new StreamGenericAbstractObjectIterator<LocalAbstractObject>(objectsClass, f.getPath()));
                }
            }
            ObjectProvidersIterator<LocalAbstractObject> input = new ObjectProvidersIterator(inputStreams);
            argIndex += 2;

            // Object output file
            File outputDir = null;
            if (args[argIndex].equals(("-out"))) {
                outputDir = new File(args[argIndex + 1]);
                if (!outputDir.isDirectory()) {
                    printUsage();
                    return;
                }
                argIndex += 2;
            }

            // Map file (photoid->userid)
            if (!args[argIndex].equals(("-users"))) {
                printUsage();
                return;
            }
            // read into memory the object id -> user map
            Map<String, String> idsUsers = new HashMap<String, String>();

            BufferedReader usersReader;
            if (args[argIndex + 1].equals("-")) {
                usersReader = new BufferedReader(new InputStreamReader(System.in));
            //System.out.println("Reading photo->user mapping from stdin...");
            } else {
                usersReader = new BufferedReader(new FileReader(args[argIndex + 1]));
            //System.out.println("Reading photo->user mapping from " + args[argIndex + 1] + "...");
            }
            int pairCnt = 0;
            for (String line = usersReader.readLine(); line != null && line.length() > 0; line = usersReader.readLine()) {
                String[] pairPhotoUser = line.trim().split("[ ,\t] *");
                if (pairPhotoUser.length != 2) {
                    System.err.println("Incorrect photo->user mapping. Delimiter can be space, comma or tab.");
                    continue;
                }
                if (pairPhotoUser[0].length() < 9) {
                    String format = String.format("%%0%dd", 9 - pairPhotoUser[0].length());
                    pairPhotoUser[0] = String.format(format, 0) + pairPhotoUser[0];
                }
                idsUsers.put(pairPhotoUser[0], pairPhotoUser[1]);
                ++pairCnt;
                if (pairCnt % 100000 == 0) {
                    System.out.println("Progress: Parsed " + idsUsers.size() + " photo->user pairs so far.");
                }
            //System.err.println("DBG: User map item: photo " + pairPhotoUser[0] + " -> user " + pairPhotoUser[1]);
            }
            System.out.println("Parsed " + idsUsers.size() + " photo->user pairs.");
            System.out.flush();
            System.err.flush();


            // Process the data and divide it
            Map<String, BufferedOutputStream> usersFiles = new HashMap<String, BufferedOutputStream>();

            int objProcessedCnt = 0;
            while (input.hasNext()) {
                // Pad the locator URI
                String photoID = input.next().getLocatorURI();
                if (photoID.length() < 9) {
                    int id = Integer.parseInt(photoID);
                    photoID = String.format("%09d", id);
                }
                String user = idsUsers.get(photoID);
                if (user == null) {
                    System.err.println("Missing mapping for this photo: " + photoID);
                    continue;
                // To check for duplicates in the collection uncomment the following.
                //} else {
                //    // Remove processed photos
                //    idsUsers.remove(photoID);
                }
                BufferedOutputStream out = usersFiles.get(user);
                if (outputDir != null) {
                    if (out == null) {
                        out = new BufferedOutputStream(new FileOutputStream(new File(outputDir, user + ".data")));
                        usersFiles.put(user, out);
                    }
                    input.getCurrentObject().write(out);
                }
                ++objProcessedCnt;
                if (objProcessedCnt % 100000 == 0) {
                    System.out.println("Progress: Scattered " + objProcessedCnt + " photos so far.");
                }
            }

            // Flush all open output files
            for (BufferedOutputStream stream : usersFiles.values()) {
                if (stream != null) {
                    stream.flush();
                }
            }

        } catch (IndexOutOfBoundsException e) {
            // Wrong argument count
            printUsage();
        }
    }
}
