# MESSIF Utility #

A large variaty of utilities for the MESSIF framework.

## Contact Person ##
Michal Batko, [homepage](https://www.muni.cz/en/people/2907-michal-batko)

Vlastislav Dohnal, [homepage](https://www.muni.cz/en/people/2952-vlastislav-dohnal)

## Licence
MESSIF library is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version. MESSIF library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details. You should have received a copy of the GNU General Public License along with this program.  If not, see http://www.gnu.org/licenses/.