
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.atomic.AtomicInteger;
import messif.objects.LocalAbstractObject;
import messif.objects.util.StreamGenericAbstractObjectIterator;
import messif.utility.Convert;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Vlastislav Dohnal (xdohnal), dohnal@fi.muni.cz, Faculty of Informatics, Masaryk University, Brno, Czech Republic
 */
public class ComputeDistance {

    protected static void printUsage() {
        System.err.println("Usage: ComputeDistance <class> -in <data file> [-cnt <number>] [-filter <dist>] [-allpairs] [-pair <pos1>:<pos2>] ...");
        System.err.println("       -cnt <number>       number of objects available in the file");
        System.err.println("       -filter <dist>      print only distance satisfying the distance:");
        System.err.println("                           if <dist> is positive (+number), distances greater than or equal to it are printed.");
        System.err.println("                           if <dist> is negative (-number), distances less than or equal to it are printed.");
        System.err.println("       -allpairs           computes the distance between all pairs in the data file (excluding symetric pairs)");
        System.err.println("       -cartesian          computes the distance between all pairs in the data file (including symetric pairs)");
        System.err.println("       -pair <pos1>:<pos2> computes the distance between objects at the given positions (zero based)");
        System.err.println("       -printlocators      print locators instead of object positions");
    }

    public static void main(String[] args) {

        if (args.length < 3) {
            printUsage();
            return;
        }
        try {
            int cnt = Integer.MAX_VALUE;
            boolean allPairs = false;
            boolean cartesian = false;
            List<Integer> pairFrom = new ArrayList<Integer>();
            List<Integer> pairTo = new ArrayList<Integer>();
            float fltDist = 0;
            boolean printLocators = false;

            // Check parameters
            for (int par = 2; par < args.length; par++) {
                if (args[par].equalsIgnoreCase("-cnt"))
                    cnt = Integer.valueOf(args[++par]);
                else if (args[par].equalsIgnoreCase("-allpairs"))
                    allPairs = true;
                else if (args[par].equalsIgnoreCase("-cartesian"))
                    cartesian = true;
                else if (args[par].equalsIgnoreCase("-pair")) {
                    String[] idxs = args[++par].split(":");
                    pairFrom.add(Integer.valueOf(idxs[0]));
                    pairTo.add(Integer.valueOf(idxs[1]));
                } else if (args[par].equalsIgnoreCase("-filter"))
                    fltDist = Float.valueOf(args[++par]);
                else if (args[par].equalsIgnoreCase("-printlocators"))
                    printLocators = true;
            }

            if ((allPairs || cartesian) && pairFrom.size() > 0) {
                System.err.println("Cannot combine -allpairs or -cartesian with -pair parameter!!!");
                printUsage();
                return;
            }

            if (! args[1].equalsIgnoreCase("-in")) {
                printUsage();
                return;
            }


            // Open sample stream
            StreamGenericAbstractObjectIterator<LocalAbstractObject> iterator = new StreamGenericAbstractObjectIterator<LocalAbstractObject>(
                    Convert.getClassForName(args[0], LocalAbstractObject.class), // Class of objects in file
                    args[2] // File name
            );

            // Read the objects in
            List<LocalAbstractObject> objs = new ArrayList<LocalAbstractObject>();
            while (iterator.hasNext() && cnt-- > 0)
                objs.add(iterator.next());


            if (allPairs || cartesian) {
                int cntI, cntJ;
                cntI = cntJ = objs.size();
                cntI--;
                for (int i = 0; i < cntI; i++) {
                    int j = (cartesian) ? 0 : i + 1;
                    for (; j < cntJ; j++) {
                        float d = objs.get(i).getDistance(objs.get(j));
                        if (fltDist >= 0) {
                            if (d >= fltDist)
//                                System.out.printf("Dist(%d-%d): %f\n", i, j, d);
                                System.out.printf("Dist(%s-%s): %f\n", 
                                        (printLocators ? objs.get(i).getLocatorURI() : i), (printLocators ? objs.get(j).getLocatorURI() : j), d);
                        } else {
                            if (-d <= fltDist)
                                System.out.printf("Dist(%s-%s): %f\n", 
                                        (printLocators ? objs.get(i).getLocatorURI() : i), (printLocators ? objs.get(j).getLocatorURI() : j), d);
                        }
                    }
                }
            } else {
                int pairs = pairFrom.size();
                for (int i = 0; i < pairs; i++) {
                    float d = objs.get(pairFrom.get(i)).getDistance(objs.get(pairTo.get(i)));
                    if (fltDist >= 0) {
                        if (d >= fltDist)
//                            System.out.printf("Dist(%d-%d): %f\n", pairFrom.get(i), pairTo.get(i), d);
                            System.out.printf("Dist(%s-%s): %f\n", 
                                    (printLocators ? objs.get(pairFrom.get(i)).getLocatorURI() : pairFrom.get(i)), 
                                    (printLocators ? objs.get(pairTo.get(i)).getLocatorURI() : pairTo.get(i)), d);
                    } else {
                        if (-d <= fltDist)
                            System.out.printf("Dist(%s-%s): %f\n", 
                                    (printLocators ? objs.get(pairFrom.get(i)).getLocatorURI() : pairFrom.get(i)), 
                                    (printLocators ? objs.get(pairTo.get(i)).getLocatorURI() : pairTo.get(i)), d);
                    }
                }
            }
        } catch (IndexOutOfBoundsException e) {
            // Wrong argument count
            printUsage();
        } catch (Throwable e) {
            System.err.println(e.toString());
        }
    }

}
