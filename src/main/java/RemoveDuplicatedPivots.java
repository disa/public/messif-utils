

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import messif.objects.util.AbstractObjectList;
import messif.objects.LocalAbstractObject;
import messif.objects.util.StreamGenericAbstractObjectIterator;
import messif.utility.Convert;
import messif.utility.SortedCollection;


/**
 * Select the pivots using some chooser and settings.
 * 
 * @author David Novak, FI Masaryk University, Brno, Czech Republic; <a href="mailto:xnovak8@fi.muni.cz">xnovak8@fi.muni.cz</a>
 */
public class RemoveDuplicatedPivots {
    
    /** Formats the usage string */
    private static String usage() {
        return "usage: RemoveDuplicatedPivots [parameters]\n" +
                " Parameters:\n" +
                "     -h [--help]         - show this help\n" +
                "     -outfile <file>     - file name to store the selected to\n" +
                "     -cls <object_class> - metric-object class to be used\n" +
                "     -np <#_of_pivots>   - number of pivots to retrieve [20]\n" +
                "     -pf <pivots_file>   - file with preselected pivots\n" +
                "     -arg <bool_arg>     - additional boolean argument for the stream object constructor\n"
                ;
    }
    
    public static void main(String args[]) {
        Class<? extends LocalAbstractObject> objectClass = null;
        int finalNumberOfPivots = 20;
        String pivotsFileName = null;
        String outputPivotsFile = null;
        String additionalArg = null;
        
        StringBuffer strbuf = new StringBuffer("The 'RemoveDuplicatedPivots' program started with arguments: ");
        for (int i = 0; i<args.length; i++)
            strbuf.append(args[i]+", ");
        System.out.println(strbuf.toString());
        
        // print usage
        if ((args.length == 0) || ((args[0].equals("-h")) || (args[0].equals("--help")))) {
            System.out.println(usage());
            return;
        }
        
        // parse and handle the options and commands
        int argCntr = 0;
        while (argCntr < args.length) {
            // the object class for this M-Chord system
            if (args[argCntr].equals("-cls")) {
                if (++argCntr >= args.length) {
                    System.err.println("Missing argument\n\n" + usage()); return;
                }
                try {
                    if (!args[argCntr].contains("."))
                        args[argCntr] = "messif.objects.impl." + args[argCntr];
                    objectClass = Convert.getClassForName(args[argCntr], LocalAbstractObject.class);
                } catch (ClassNotFoundException e) {
                    System.err.println("Unknown class "+args[argCntr]+"\n\n"+usage());
                    return;
                } catch (ClassCastException e) {
                    System.err.println("Class "+args[argCntr]+" must extend LocalAbstractObject\n\n"+usage());
                    return;
                }
                if (!LocalAbstractObject.class.isAssignableFrom(objectClass)) {
                    System.err.println("Class "+args[argCntr]+" is not subclass of messif.objects.LocalAbstractObject\n\n"+usage());
                    return;
                }
            }
            
            // number of pivots
            else if (args[argCntr].equals("-np")) {
                if (++argCntr >= args.length) {
                    System.err.println("Missing argument\n\n" + usage()); return;
                }
                finalNumberOfPivots = Integer.parseInt(args[argCntr]);
            }
            // file with preselected pivots (pivot file)
            else if (args[argCntr].equals("-pf")) {
                if (++argCntr >= args.length) {
                    System.err.println("Missing argument\n\n" + usage()); return;
                }
                pivotsFileName = args[argCntr];
            }
            // configuration file specification
            else if (args[argCntr].equals("-outfile")) {
                if (++argCntr >= args.length) {
                    System.err.println("Missing argument\n\n" + usage()); return;
                }
                outputPivotsFile = args[argCntr];
            }
            // additional bool arg
            else if (args[argCntr].equals("-arg")) {
                if (++argCntr >= args.length) {
                    System.err.println("Missing argument\n\n" + usage()); return;
                }
                additionalArg = args[argCntr];
            }
            // else: unknown option
            else {
                System.err.println("Unknown option/command '"+args[argCntr]+"'\n\n" + usage());
                return;
            }
            argCntr++;
        } // end of parameters parsing
        
                
        // read the preselected pivots if specified
        AbstractObjectList<LocalAbstractObject> pivots = null;
        if (pivotsFileName != null) {
            pivots = new AbstractObjectList<LocalAbstractObject>();
            try {
                // Open sample stream
                StreamGenericAbstractObjectIterator<LocalAbstractObject> iterator;
                if (additionalArg == null) {
                    iterator = new StreamGenericAbstractObjectIterator<LocalAbstractObject>(objectClass, pivotsFileName);
                } else {
//                    List<Boolean> constructorArgs = Collections.singletonList(Boolean.valueOf(additionalArg));
                    List<Boolean> constructorArgs = Collections.singletonList(Boolean.valueOf(additionalArg));
                    iterator = new StreamGenericAbstractObjectIterator<LocalAbstractObject>(objectClass,
                            pivotsFileName, null, constructorArgs);
                }
                
                while (iterator.hasNext()) {
                    pivots.add(iterator.next());
                }
            } catch (IOException e) {
                e.printStackTrace(); return;
            }
        }
        int numberOfPivots = pivots.size();

        // Filter out the close pivots
        //LocalAbstractObject pivot = null;
        SortedCollection<DistancePivotPair> pivotPivotDistances = new SortedCollection<DistancePivotPair>();
        try {
            /** Matrix of mutual pivot distances */
//            float [] [] pivotDistancesMatrix = new float[numberOfPivots][numberOfPivots];
            for (int i = 0; i < numberOfPivots; i++) {
                for (int j = 0; j < i; j++) {
//                    pivotDistancesMatrix[i][j] = pivots.get(i).getDistance(pivots.get(j));
//                    pivotDistancesMatrix[j][i] = pivotDistancesMatrix[i][j];
                    pivotPivotDistances.add(new DistancePivotPair(pivots.get(i).getDistance(pivots.get(j)), i, j));
                }
//                pivotDistancesMatrix[i][i] = 0f;
            }

            // main filtering loop
            int i = finalNumberOfPivots;
            Set<Integer> removedPivots = new HashSet<Integer>();
            while (i < numberOfPivots) {
                DistancePivotPair smallestDist;
                do {
                    smallestDist = pivotPivotDistances.removeLast();
                } while (removedPivots.contains(smallestDist.index1) || removedPivots.contains(smallestDist.index2));
                System.out.println("Removing pivot " + smallestDist.index1 + " because of distance to pivot "+ smallestDist.index2+": "+ smallestDist.distance);
                //pivots.remove(smallestDist.index1);
                removedPivots.add(smallestDist.index1);
                i++;
            }

            FileOutputStream outputFile = new FileOutputStream(outputPivotsFile);
            // print the result
            for (int j = 0; j < pivots.size(); j++) {
                if (! removedPivots.contains(j)) {
                    pivots.get(j).write(outputFile);
                }
            }
            outputFile.close();
            System.out.println("Pivots selected");
            
        } catch (IOException f) {
            System.err.println("Error writing configuration file");
        }
    }

    protected static class DistancePivotPair implements Comparable<DistancePivotPair> {

        protected final float distance;

        protected final int index1;

        protected final int index2;

        public DistancePivotPair(float distance, int index1, int index2) {
            this.distance = distance;
            this.index1 = index1;
            this.index2 = index2;
        }

        @Override
        public int compareTo(DistancePivotPair o) {
            return Float.compare(o.distance, distance);
        }
    }
}
