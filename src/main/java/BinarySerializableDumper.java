
import java.io.File;
import java.io.PrintStream;

/*
 *  BinarySerializableDumper
 * 
 */
import java.util.HashMap;
import java.util.Map;
import messif.buckets.index.ModifiableSearch;
import messif.buckets.storage.impl.DiskStorage;

/**
 *
 * @author xbatko
 */
public class BinarySerializableDumper {

    public static void usage(PrintStream out) {
        out.println("Usage: BinarySerializableDumper <storedObjectClass> [<cacheClasses> [<bufferSize> [<directBuffer> [<memoryMap> [<startPosition>]]]]] <file> [<file> ...]");
        out.println("  cacheClasses - comma-separated list of classes that will be cached");
        out.println("  bufferSize - the size of the buffers used for I/O operations");
        out.println("  directBuffer - true/false - use faster direct buffers for I/O operations");
        out.println("  startPosition - the position (in bytes) of the first block of the data");
    }

    public static void main(String[] args) throws ClassNotFoundException {
        //Logger.setConsoleLevel(Level.ALL);
        //Logger.setLogLevel(Level.ALL);

        // Read parameters
        Map<String, Object> params = new HashMap<String, Object>();
        //params.put("readOnly", "true");
        String[] argNames = {"cacheClasses", "bufferSize", "directBuffer", "memoryMap", "startPosition"};
        int argInd = 1;
        for (;argInd < Math.min(6, args.length); argInd++) {
            if (new File(args[argInd]).exists())
                break;
            params.put(argNames[argInd - 1], args[argInd]);
        }

        // Not enough parameters
        if (argInd >= args.length) {
            usage(System.err);
            return;
        }

        // First parameter is the class
        Class<?> storedClass = Class.forName(args[0]);
        
        // Process files
        for (;argInd < args.length; argInd++) {
            // Set file name parameter
            params.put("file", args[argInd]);

            try {
                // Create storage wrapper
                long openTime = System.currentTimeMillis();
                DiskStorage<?> storage = DiskStorage.create(storedClass, params);

                // Initialize search
                long readTime = System.currentTimeMillis();
                openTime = readTime - openTime;
                ModifiableSearch<?> search = storage.search();

                // Read all objects
                long dumpTime = System.currentTimeMillis();
                readTime = dumpTime - readTime;
                int count = 0;
                while (search.next()) {
                    System.out.println(search.getCurrentObject());
                    count++;
                }
                dumpTime = System.currentTimeMillis() - dumpTime;

                System.err.println(
                        "File: " + args[argInd] +
                        ", OpenTime: " + openTime +
                        ", ReadTime: " + readTime +
                        ", DumpTime: " + dumpTime +
                        ", ObjectCount: " + count +
                        ", Fragmentation: " + storage.getFragmentation());
                storage.finalize();
            } catch (Throwable e) {
                System.err.println("Error processing " + args[argInd] + ": " + e.toString());
                e.printStackTrace();
            }
        }
    }
}
