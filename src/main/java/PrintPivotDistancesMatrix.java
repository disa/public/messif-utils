
import java.io.File;
import java.io.FileInputStream;
import java.io.ObjectInputStream;
import messif.utility.ExtendedProperties;
import mindex.MetricIndex;


/**
 *
 * @author xnovak8
 */
public class PrintPivotDistancesMatrix {

    private static void printUsage() {
        System.err.println("Usage: PrintPivotDistancesMatrix <mindex>[.properties]");
        System.err.println("   Creates the M-index (either deserialization or from .properties file)");
        System.err.println("   and calculates distances between all pairs of pivots and print them");
    }
    // M-Index configuration
    private static MetricIndex mIndex = null;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {

            /************ Parsing arguments ****************/
            int paramInd = 0;

            if (args.length < 1) {
                printUsage();
                return;
            }

            // Read MChord configuration
            String mIndexFile = args[paramInd++];
            if (mIndexFile.endsWith(".properties")) {
                ExtendedProperties extendedProperties = new ExtendedProperties();
                extendedProperties.load(new FileInputStream(new File(mIndexFile)));
                mIndex = new MetricIndex(extendedProperties, "mindex.");
            } else {
                ObjectInputStream configInput = new ObjectInputStream(new FileInputStream(mIndexFile));
                mIndex = (MetricIndex) configInput.readObject();
                configInput.close();
            }

            System.err.print("Configuration: ");
            System.err.println(mIndex);

            StringBuffer buf = new StringBuffer("\\begin{tabular}{|r|");
            for (int i = 0; i < mIndex.getNumberOfPivots();  i++ ) {
                buf.append("c|");
            }
            buf.append("}\n\\hline\n & {\\bfseries 0}");
            for (int i = 1; i < mIndex.getNumberOfPivots(); i++) {
                buf.append(" & {\\bfseries ").append(i).append("}");
            }
            buf.append("\\\\ \\hline\\hline\n");
            for (int i = 0; i < mIndex.getNumberOfPivots(); i++) {
                buf.append("{\\bfseries ").append(i).append("}");
                for (int j = 0; j < mIndex.getNumberOfPivots(); j++) {
                    buf.append(" & ").append(String.format("%1.2f", mIndex.getPivotsDistance(i,j)));
                }
                buf.append("\\\\ \\hline\n");
            }
            buf.append("\\end{tabular}\n");

            System.out.println(buf.toString());

        } catch (Throwable e) {
            e.printStackTrace();
            printUsage();
        }
    }
}
