/*
 * IncreaseDataset.java
 *
 * Created on May 9, 2007
 */

import messif.objects.keys.AbstractObjectKey;
import messif.objects.LocalAbstractObject;
import messif.objects.util.StreamGenericAbstractObjectIterator;
import messif.utility.Convert;

/**
 * This class serves to increase given dataset by given factor randomly.
 *
 * @author David Novak, FI Masaryk University, Brno, Czech Republic; <a href="mailto:xnovak8@fi.muni.cz">xnovak8@fi.muni.cz</a>
 */
public class IncreaseDataset {
        
    /**
     * The entry point to the program to randomly increase given dataset by given factor.
     * <pre>
     * Usage: IncreaseDataset &lt;class&gt; &lt;increase_factor&gt; &lt;data_file&gt; &lt;min_object&gt; &lt;max_object&gt;
     * Increases the dataset in a random by given increase factor and print it to stdout. The limits are given.
     * Params:
     *          class            the class of the data objects; extends LocalAbstractObject
     *          increase_factor  factor to increase the dataset by
     *          data_file        file with the dataset
     *          min_object       file containing the object with the minimal values for random increasing
     *          max_object       file containing the object with the maximal values for random increasing
     * </pre>
     * @param args command line arguments - see above
     */
    public static void main(String[] args) {
        if (args.length < 5) {
            StringBuffer buffer = new StringBuffer("Usage: IncreaseDataset <class> <increase_factor> <data_file> <min_object> <max_object>\n");
            buffer.append("Increases the dataset in a random by given increase factor and print it to stdout. The limits are given.\n");
            buffer.append("Params:\n");
            buffer.append("\t class            the class of the data objects; extends LocalAbstractObject\n");
            buffer.append("\t increase_factor  factor to increase the dataset by\n");
            buffer.append("\t data_file        file with the dataset\n");
            buffer.append("\t min_object       file containing the object with the minimal values for random increasing\n");
            buffer.append("\t max_object       file containing the object with the maximal values for random increasing\n");
            System.err.println(buffer.toString());
            return;
        }
        try {
            /************   Parsing arguments ****************/
            // The class of the dataset - extends LocalAbstractObject
            Class<? extends LocalAbstractObject> objectClass = Convert.getClassForName(args[0], LocalAbstractObject.class);
            
            int increaseFactor = Integer.valueOf(args[1]);
            
            String dataFile = args[2];
            
            String minObjectFile = args[3];
            
            String maxObjectFile = args[4];
            
            // read the data into memory
            StreamGenericAbstractObjectIterator<LocalAbstractObject> iterator = 
                    new StreamGenericAbstractObjectIterator<LocalAbstractObject>(objectClass, dataFile);
            
            // read the minimal and maximal object
            LocalAbstractObject minObject = (new StreamGenericAbstractObjectIterator<LocalAbstractObject>(objectClass, minObjectFile)).next();
            LocalAbstractObject maxObject = (new StreamGenericAbstractObjectIterator<LocalAbstractObject>(objectClass, maxObjectFile)).next();
            
            // Increase the dataset
            LocalAbstractObject object;
            LocalAbstractObject randomObject; 
            int objCount = 0;
            for ( ; iterator.hasNext(); ) {
                object = iterator.next();
                object.write(System.out);
                
                for (int i = 1; i < increaseFactor; i++) {
                    randomObject = object.cloneRandomlyModify(minObject, maxObject);
                    randomObject.setObjectKey(new AbstractObjectKey(randomObject.getLocatorURI().toString()+"_"+i));
                    randomObject.write(System.out);
                    System.err.println("Distance: "+randomObject.getDistance(object));
                }
                
                if (++objCount % 1000 == 0)
                    System.err.println("Objects processed: "+objCount);
            }
            
            System.err.println("Processed "+objCount+" objects; output: "+(objCount * increaseFactor)+" objects");
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }
}
