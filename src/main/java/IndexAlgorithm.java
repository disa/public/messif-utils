import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import messif.algorithms.Algorithm;
import messif.algorithms.impl.SequentialScan;
import messif.buckets.LocalBucket;
import messif.buckets.impl.PlainStorageBucket;
import messif.buckets.index.ModifiableIndex;
import messif.objects.LocalAbstractObject;
import messif.operations.QueryOperation;

/**
 * Algorithm that encapsulates {@link ModifiableIndex}.
 * This algorithm supports only {@link QueryOperation}s and they are executed
 * directly on the index.
 *
 * @author xbatko
 * @deprecated Use standard {@link SequentialScan} algorithm with {@link PlainStorageBucket}
 */
public class IndexAlgorithm extends Algorithm {
    /** class id for serialization */
    private static final long serialVersionUID = 1L;

    /** Bucket that encapsulates the index */
    private final LocalBucket bucket;

    /**
     * Creates a new IndexAlgorithm that wraps the given index.
     * @param index the index to wrap
     */
    public IndexAlgorithm(final ModifiableIndex<LocalAbstractObject> index) {
        super("Index algorithm");
        this.bucket = new LocalBucket(Long.MAX_VALUE, Long.MAX_VALUE, 0, false, 0L) {
            /** class id for serialization */
            private static final long serialVersionUID = 1L;
            @Override
            protected ModifiableIndex<LocalAbstractObject> getModifiableIndex() {
                return index;
            }
        };
    }

    /**
     * Creates a new IndexAlgorithm that wraps the given index.
     * @param fileName the name of the file where the index is serialized
     * @throws IOException if there was an error deserializing the index from the given file
     * @throws ClassNotFoundException if there was an error deserializing the index from the given file
     */
    @AlgorithmConstructor(description="Deserialize index from file", arguments={"file name"})
    public IndexAlgorithm(String fileName) throws IOException, ClassNotFoundException {
        this(deserializeIndex(fileName));
    }

    /**
     * Deserializes index from the given file.
     * @param fileName the name of the file where the index is serialized
     * @return the deserialized index
     * @throws IOException if there was an error deserializing the index from the given file
     * @throws ClassNotFoundException if there was an error deserializing the index from the given file
     */
    @SuppressWarnings("unchecked")
    private static ModifiableIndex<LocalAbstractObject> deserializeIndex(String fileName) throws IOException, ClassNotFoundException {
        ObjectInputStream stream = new ObjectInputStream(new FileInputStream(fileName));
        try {
            return (ModifiableIndex)stream.readObject();
        } finally {
            stream.close();
        }
    }

    /**
     * Executes any query operation on the encapsulated index.
     * @param operation the operation to execute
     */
    public void executeQuery(QueryOperation<?> operation) {
        bucket.processQuery(operation);
        operation.endOperation();
    }
}
