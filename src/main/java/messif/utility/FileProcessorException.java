package messif.utility;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Indicates one or more errors that occurred during file processing.
 * @author xbatko
 * @see FileSearchProcess
 * @see FileProcessor
 */
public class FileProcessorException extends Exception {
    /** Version of this class for serialization */
    private static final long serialVersionUID = 1L;

    /** List of stored exceptions */
    private final Map<String, Exception> exceptions = new LinkedHashMap<String, Exception>();

    /**
     * Creates a new instance of <code>FileProcessorException</code>
     * for the encapsulated exception on the given file.
     * @param source the file the processing of which caused an exception
     * @param exception the exception thrown when processing the file 
     */
    public FileProcessorException(String source, Exception exception) {
        addException(source, exception);
    }

    /**
     * Adds an exception for a given file to the given {@code currentException}.
     * If the {@code currentException} is <tt>null</tt>, a new instance is created.
     * @param currentException the current exception list to add the new exception to
     * @param source the file the processing of which caused an exception
     * @param exception the exception thrown when processing the file
     * @return the passed currentException or, if it was <tt>null</tt> a new exception instance
     */
    public static FileProcessorException addException(FileProcessorException currentException, String source, Exception exception) {
        if (currentException == null)
            return new FileProcessorException(source, exception);
        currentException.addException(source, exception);
        return currentException;
    }

    /**
     * Adds an exception for a given file.
     * @param source the file the processing of which caused an exception
     * @param exception the exception thrown when processing the file
     */
    public final void addException(String source, Exception exception) {
        if (exceptions.put(source, exception) != null)
            throw new IllegalArgumentException("File " + source + " already has a stored exception");
    }

    /**
     * Returns all encapsulated exceptions.
     * @return all encapsulated exceptions
     */
    public Map<String, Exception> getExceptions() {
        return Collections.unmodifiableMap(exceptions);
    }

    @Override
    public String toString() {
        StringBuilder str = new StringBuilder();
        for (Map.Entry<String, Exception> entry : exceptions.entrySet()) {
            str.append(entry.getKey()).append(": ").append(entry.getValue()).append("\n");
        }
        return str.toString();
    }

}
