
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.io.PrintStream;

/*
 *  BinarySerializableDumper
 * 
 */
import java.util.HashMap;
import java.util.Map;
import messif.buckets.index.LocalAbstractObjectOrder;
import messif.buckets.index.ModifiableOrderedIndex;
import messif.buckets.index.impl.LongStorageIndex;
import messif.buckets.storage.impl.DiskStorage;
import messif.objects.LocalAbstractObject;
import messif.objects.util.StreamGenericAbstractObjectIterator;
import messif.objects.util.StreamsMetaObjectMapIterator;
import messif.utility.Convert;

/**
 *
 * @author xbatko
 */
public class LocatorStorageMetaMapCreator {

    public static void usage(PrintStream out) {
        out.println("Usage: LocatorStorageMetaMapCreator <cacheClasses> <out_storage_file> <out_index_file> [[\"<subdistance_name class file>\"]...]");
        out.println("      create locator-indexed storage with MetaObjectMaps created from several files");
        out.println("  subdistance_name - name of the sub-objects created in the MetaObjectMap");
        out.println("  class - class of sub-objects that are created in the MetaObjectMap");
        out.println("  file - fil with text representation of the sub-objects that are created in the MetaObjectMap");
        out.println("  cacheClasses - comma-separated list of classes that will be cached");
    }

    public static void main(String[] args) throws Throwable {
        try {
            if (args.length < 4) {
                usage(System.err);
                System.exit(1);
            }

            // prepare parameters for the disk storage
            Map<String, Object> params = new HashMap<String, Object>();
            String[] argNames = {"readOnly", "cacheClasses", "bufferSize", "directBuffer", "memoryMap", "startPosition", "file"};
            Object[] argValues = {false, args[0], 16*1024, true, false, 0L, args[1]};
            for (int i = 1; i < argNames.length; i++) {
                params.put(argNames[i], argValues[i]);
            }

            DiskStorage<LocalAbstractObject> diskStorage = DiskStorage.create(LocalAbstractObject.class, params);

            ModifiableOrderedIndex<String, LocalAbstractObject> index = new LongStorageIndex<String, LocalAbstractObject>(
                    diskStorage, LocalAbstractObjectOrder.locatorToLocalObjectComparator);

            // read the objects in
            StreamsMetaObjectMapIterator iterator = new StreamsMetaObjectMapIterator();
            for (int argIndex = 3; argIndex < args.length; argIndex++) {
                String[] nameClassFile = args[argIndex].split(" ", 3);
                iterator.addObjectStream(
                        nameClassFile[0],
                        Convert.getClassForName(nameClassFile[1], LocalAbstractObject.class),
                        nameClassFile[2]);
            }

            while (iterator.hasNext()) {
                index.add(iterator.next());
            }

            ObjectOutputStream indexOutput = new ObjectOutputStream(new FileOutputStream(args[2]));
            indexOutput.writeObject(index);

            index.destroy();
            
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }
}
