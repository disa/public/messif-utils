
import java.io.File;
import java.io.PrintStream;

/*
 *  BinarySerializableDumper
 * 
 */
import java.util.Random;
import messif.buckets.impl.DiskBlockBucket;
import messif.objects.LocalAbstractObject;
import messif.objects.nio.BinarySerializator;
import messif.objects.nio.CachingSerializator;
import messif.objects.util.AbstractObjectIterator;
import messif.utility.Convert;

/**
 *
 * @author xbatko
 */
public class DiskBlockBucketRandomDumper {

    public static void usage(PrintStream out) {
        out.println("Usage: DiskBlockBucketDumper <class> <cacheClasses> <file> [<randomCount>]");
        out.println("  class - the class of objects that are stored in the bucket");
        out.println("  cacheClasses - comma-separated list of classes that will be cached");
    }

    public static void main(String[] args) throws Throwable {
        //Logger.setConsoleLevel(Level.ALL);
        //Logger.setLogLevel(Level.ALL);

        if (args.length < 3) {
            usage(System.err);
            System.exit(1);
        }

        BinarySerializator serializator = new CachingSerializator<Object>(Class.forName(args[0]), Convert.stringToType(args[1], Class[].class));
        DiskBlockBucket bucket = new DiskBlockBucket(Long.MAX_VALUE, Long.MAX_VALUE, 0, new File(args[2]), 16*1024, true, 128, serializator);

        try {
            
            AbstractObjectIterator<LocalAbstractObject> objects = bucket.getAllObjects();
        
            if (args.length == 4) {
                int randomCount = Integer.parseInt(args[3]);
                int pool = bucket.getObjectCount() / randomCount;
                Random randGenerator = new Random();
                int iter = 0;

                do {
                    int randPool = randGenerator.nextInt(pool);
                    iter++;
                    System.err.println("Selecting " + randPool + " from " + iter);
                    objects.getObjectByPosition(randPool).write(System.out);
                    objects.getObjectByPosition(pool - randPool);
                } while (objects.hasNext());
            } else {
                while (objects.hasNext())
                    objects.next().write(System.out);
            }
        } finally {
            bucket.finalize();
        }
    }
}
