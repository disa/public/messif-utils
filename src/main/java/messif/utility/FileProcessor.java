package messif.utility;

import java.io.InputStream;

/**
 * Interface for generic processing of files found by {@link FileSearchProcess}.
 * 
 * @author xbatko
 */
public interface FileProcessor {
    /**
     * Process the file data.
     * @param source the name of the source that provided the file data (can be <tt>null</tt>)
     * @param fileData the data to process
     * @return number of items processed from a given source or zero if there was nothing to process
     * @throws FileProcessorException if there was an error processing the file data
     */
    int processFile(String source, InputStream fileData) throws FileProcessorException;
}
