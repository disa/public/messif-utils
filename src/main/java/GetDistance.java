/*
 * DistanceDistribution.java
 *
 * Created on 16. unor 2007, 11:05
 */

import java.io.PrintStream;
import java.util.Iterator;
import java.util.Map;
import messif.objects.LocalAbstractObject;
import messif.objects.impl.MetaObjectMap;
import messif.objects.util.StreamGenericAbstractObjectIterator;
import messif.utility.Convert;

/**
 * Utility for computing distance between two (or more) objects in different files.
 *
 * @author xbatko
 */
public class GetDistance {

    public static void printUsage(PrintStream stream) {
        stream.println("Usage: GetDistance <class> <data file1> [<data file2 ...>]");
    }

    public static void printDistance(LocalAbstractObject obj1, LocalAbstractObject obj2, PrintStream printStream) {
        if (obj1 instanceof MetaObjectMap && obj2 instanceof MetaObjectMap) {
            Map<String, LocalAbstractObject> obj1Map = ((MetaObjectMap)obj1).getObjectMap();
            Map<String, LocalAbstractObject> obj2Map = ((MetaObjectMap)obj2).getObjectMap();
            for (Map.Entry<String, LocalAbstractObject> entry : obj1Map.entrySet()) {
                LocalAbstractObject obj2object = obj2Map.get(entry.getKey());
                if (obj2object != null) {
                    printStream.print(" ");
                    printStream.print(entry.getValue().getDistance(obj2object));
                    printStream.print(" [");
                    printStream.print(entry.getKey());
                    printStream.print("]");
                }
            }
        } else {
            printStream.print(" ");
            printStream.print(obj1.getDistance(obj2));
        }
    }

    public static void main(String[] args) {
        
        if (args.length < 2) {
            printUsage(System.err);
            return;
        }

        try {
            Class<LocalAbstractObject> objectClass = Convert.getClassForName(args[0], LocalAbstractObject.class);
            Iterator<LocalAbstractObject> objects1 = new StreamGenericAbstractObjectIterator<LocalAbstractObject>(objectClass, args[1]);
            Iterator<LocalAbstractObject> objects2 = args.length <= 2 ? null :
                    new StreamGenericAbstractObjectIterator<LocalAbstractObject>(objectClass, args[2]);

            if (!objects1.hasNext()) {
                System.err.println("Not enough objects in " + args[1]);
                return;
            }

            LocalAbstractObject obj1 = objects1.next();
            // Compute distance
            while (objects1.hasNext() && (objects2 == null || objects2.hasNext())) {
                LocalAbstractObject obj2 = objects2 == null ? objects1.next() : objects2.next();

                System.out.print(obj1.getLocatorURI());
                System.out.print(" to ");
                System.out.print(obj2.getLocatorURI());
                System.out.print(":");
                printDistance(obj1, obj2, System.out);
                System.out.println();
                if (objects2 != null)
                    obj1 = objects1.next();
            }
        } catch (Exception e) {
            System.err.println(e.toString());
        }
    }
}
