
import com.ice.tar.TarEntry;
import com.ice.tar.TarInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.GZIPInputStream;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * This class reads CoPhIR meta object data as XML documents and extracts nodes satisfying the passed XPath expression.
 *
 * @author Vlastislav Dohnal (xdohnal), dohnal@fi.muni.cz, Faculty of Informatics, Masaryk University, Brno, Czech Republic
 */
public class CophirMetaObjectXPathFilter {
    private static void usage() {
        System.err.println("Usage: CophirMetaObjectXPathFilter [-filter <xpath_expression>] [--] <dir|xmlfile|tarfile> [<dir|xmlfile|tarfile> ...]");
        System.err.println("    -filter ... each line of output (stdout) contains a list of Nodes identified by this XPath expression");
        System.err.println("    -- ... used to separate options from file name (if a filename start with hyphen)");
        System.err.println("If -filter argument is _not_ passed, new separate output files (<descriptor_name>.data) for individual CoPhIR descriptors are created contains textual representation of all CoPhIR meta objects read in.");
        System.err.println();
        System.err.println("Example: CophirMetaObjectXPathFilter -filter \"//MediaUri/text()|//owner/@nsid|//owner/@username\" .");
    }

    /**
     * Main method
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            int fileArg = -1;
            String xPathStr = null;
            XPathExpression xPathExpr = null;

            // Parse arguments
            for (int i = 0; i < args.length; i++) {
                if (args[i].startsWith("--") || !args[i].startsWith("-")) {       // File arguments from here
                    fileArg = i;
                    break;
                }
                // Parse an argument
                if (args[i].equals("-filter")) {
                    // Next argument is an XPath expression
                    try {
                        xPathStr = args[++i];
                        xPathExpr = getXPathExpression(xPathStr);
                    } catch (XPathExpressionException ex) {
                        System.err.println("Incorrect XPath expression");
                        ex.printStackTrace();
                        return ;
                    }
                } else {
                    // Unsupported argument
                    System.err.println("Unsupported argument has been passed: " + args[i]);
                    usage();
                    return ;
                }
            }

            if (fileArg < 0 || xPathExpr == null) {
                if (fileArg < 0)
                    System.err.println("No input file or directory has been passed.");
                if (xPathExpr == null)
                    System.err.println("No XPath expression has been passed.");
                usage();
                return;
            }

            System.out.print("# ");
            System.out.println(xPathStr);
            for (int i = fileArg; i < args.length; i++) {
                File argFile = new File(args[i]);
                if (argFile.isDirectory())
                    searchDir(argFile, xPathExpr);
                else if (args[i].matches(".*\\.(tar|tar\\.gz|tgz)$"))
                    searchTAR(argFile, xPathExpr);
                else if (args[i].matches(".*\\.xml$"))
                    searchFile(argFile, xPathExpr);
                else
                    System.err.println("Unsupported file passed! Ignoring.");
                    
            }
            System.out.flush();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Recursively search directories for XML files. For every file the XPath expression is evaluated and
     * matching nodes writted to output.
     * 
     * @param dir the directory to start the search from
     * @param xPathExpression the XPath expression used to select nodes from the document
     * @return number of files processed
     */
    public static int searchDir(File dir, XPathExpression xPathExpression) {
        int rtv = 0;
        for (File file : dir.listFiles()) {
            try {
                if (file.isDirectory()) {
                    rtv += searchDir(file, xPathExpression);
                } else if (file.getName().toLowerCase().endsWith(".xml")) {
                    // Evaluate the XPath expression and write out
                    writeNodes(xmlGetByXPath(xPathExpression, xmlToDOM(file), false));
                    rtv++;
                }
            } catch (Exception e) {
                System.err.print("Error processing '");
                System.err.print(file);
                System.err.print("': ");
                System.err.println(e);
                e.printStackTrace();
            }
        }
        return rtv;
    }

    /**
     * Check the passed file containing an XML document, evaluate the XPath expression and
     * write out the matching nodes to output.
     *
     * @param file the file containing XML document
     * @param xPathExpression the XPath expression used to select nodes from the document
     * @return number of files processed
     */
    public static int searchFile(File file, XPathExpression xPathExpression) {
        try {
            if (file.getName().toLowerCase().endsWith(".xml")) {
                // Evaluate the XPath expression and write out
                writeNodes(xmlGetByXPath(xPathExpression, xmlToDOM(file), false));
            }
        } catch (Exception e) {
                System.err.print("Error processing '");
                System.err.print(file);
                System.err.print("': ");
                System.err.println(e);
                e.printStackTrace();
        }
        return 1;
    }

    /**
     * Check the whole content of an TAR file. For every file, the XPath expression is evaluated and
     * matching nodes writted to output.
     *
     * @param file the TAR archive file
     * @param xPathExpression the XPath expression used to select nodes from the document
     * @return number of meta objects processed
     * @throws IOException if there was an error reading TAR file
     */
    public static int searchTAR(File file, XPathExpression xPathExpression) throws IOException {
        // Open tar file (with decompression if needed)
        InputStream stream = new FileInputStream(file);
        if (file.getName().endsWith("gz"))
            stream = new GZIPInputStream(stream);
        TarInputStream tar = new TarInputStream(stream) {
            // This is necessary since SAXParser calls close of input stream after processing :-((
            @Override
            public void close() throws IOException {
            }
            public void finalClose() throws IOException {
                super.close();
            }
        };
        int rtv = 0;

        // Read all entries in the tar file
        for (TarEntry entry = tar.getNextEntry(); entry != null; entry = tar.getNextEntry()) {
            // Ignore entries without ".xml" suffix
            if (!entry.getName().toLowerCase().endsWith(".xml") || entry.isDirectory())
                continue;

            try {
                // Evaluate the XPath expression and write out
                writeNodes(xmlGetByXPath(xPathExpression, xmlToDOM(entry), false));

                rtv++;
            } catch (Exception e) {
                System.err.print("Error processing '");
                System.err.print(file);
                System.err.print("': ");
                System.err.println(e);
                e.printStackTrace();
            }
        }

        return rtv;
    }

    private static void writeNodes(List<Node> xmlGetByXPath) {
        int cnt = xmlGetByXPath.size();

        for (Node n : xmlGetByXPath) {
            cnt--;
//            if (n.getNodeType() == Node.TEXT_NODE)
//                System.out.print(n.getParentNode().getNodeName());
//            else
//                System.out.print(n.getNodeName());
//            System.out.print("=");
            System.out.print(n.getTextContent());
            if (cnt > 0)
                System.out.print(", ");
            else
                System.out.println();
        }
    }

    //****************** XML utilities ******************//

    /** XPath expressions factory */
    private static XPath xPath;
    /** Factory for creating DOM documents from XML data */
    private static DocumentBuilder documentBuilder;

    /**
     * Returns the factory for creating DOM documents from XML data.
     * The instance is created on first use and reused on subsequent calls.
     * This call is thread-safe.
     * @return the factory for creating DOM documents from XML data
     * @throws ParserConfigurationException if there was an error creating a new document builder
     */
    public static DocumentBuilder getDocumentBuilder() throws ParserConfigurationException {
        if (documentBuilder != null)
            return documentBuilder;
        synchronized (CophirMetaObjectXPathFilter.class) {
            if (documentBuilder == null)
                documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            return documentBuilder;
        }
    }

    /**
     * Returns the XPath expressions factory.
     * The instance is created on first use and reused on subsequent calls.
     * This call is thread-safe.
     * @return the XPath expressions factory
     */
    private static XPath getXPath() {
        if (xPath != null)
            return xPath;
        synchronized (CophirMetaObjectXPathFilter.class) {
            if (xPath == null)
                xPath = XPathFactory.newInstance().newXPath();
            return xPath;
        }
    }

    /**
     * Returns a new compiled XPath expression.
     * This call is thread-safe.
     * @param expression the XPath expression string
     * @return a new compiled XPath expression
     * @throws XPathExpressionException if there was a problem parsing the string
     */
    public static XPathExpression getXPathExpression(String expression) throws XPathExpressionException {
        final XPath factory = getXPath();

        synchronized (factory) {
            return factory.compile(expression);
        }
    }

    /**
     * Parse a given XML document into an object model (DOM).
     * The XML document can be passed in the corresponding argument as
     * a {@link File}, an {@link InputStream}, or a URI (as {@link String}).
     *
     * @param xmlSource identification of XML document source
     * @return a parsed document
     * @throws ParserConfigurationException if there was an error initializing the parser
     * @throws SAXException if there was an error parsing the document
     * @throws IOException if there was an error reading the source from {@code uri}
     * @throws IllegalArgumentException if unsuppoted type of XML document source is passed.
     * @see DocumentBuilder#parse(java.lang.String)
     */
    public static Document xmlToDOM(Object xmlSource) throws ParserConfigurationException, SAXException, IOException, IllegalArgumentException {
        if (xmlSource instanceof String)
            return getDocumentBuilder().parse((String)xmlSource);
        else if (xmlSource instanceof InputStream)
            return getDocumentBuilder().parse((InputStream)xmlSource);
        else if (xmlSource instanceof File)
            return getDocumentBuilder().parse((File)xmlSource);
        else
            throw new IllegalArgumentException("This type of argument is not supported. You can use: String, File, InputStream.");
    }

    /**
     * Returns a list of nodes selected by the given XPath expression from the given XML document.
     * This call is thread-safe with respect to the XPath expression if {@code synchronizeExpression} is <tt>true</tt>.
     *
     * @param xPathExpression the XPath expression used to select nodes from the document
     * @param document a XML document object model
     * @param synchronizeExpression if <tt>true</tt>, the XPath expression parsing is synchronized on the expression
     * @return a list of {@link Node nodes} from the given {@code document}
     * @throws XPathExpressionException if there was an error evaluating the expression on the document
     */
    public static List<Node> xmlGetByXPath(XPathExpression xPathExpression, Document document, boolean synchronizeExpression) throws XPathExpressionException {
        NodeList nodes;
        if (synchronizeExpression) {
            synchronized (xPathExpression) {
                nodes = (NodeList)xPathExpression.evaluate(document, XPathConstants.NODESET);
            }
        } else {
            nodes = (NodeList)xPathExpression.evaluate(document.getFirstChild(), XPathConstants.NODESET);
        }
        List<Node> ret = new ArrayList<Node>();
        for (int i = 0; i < nodes.getLength(); i++)
            ret.add(nodes.item(i));
        return ret;
    }
}
