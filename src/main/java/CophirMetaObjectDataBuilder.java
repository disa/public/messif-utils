import java.io.BufferedOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.GZIPOutputStream;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import messif.buckets.storage.IntStorageIndexed;
import messif.buckets.storage.impl.DatabaseStorage;
import messif.objects.LocalAbstractObject;
import messif.objects.MetaObject;
import messif.objects.impl.MetaObjectMap;
import messif.objects.impl.CophirXmlParser;
import messif.objects.impl.MetaObjectCophirKeywords;
import messif.objects.text.Stemmer;
import messif.utility.FileProcessor;
import messif.utility.FileSearchProcess;
import messif.utility.reflection.InstantiatorSignature;
import org.xml.sax.SAXException;


/**
 * Recursively scans through the directories provided as command-line arguments and
 * creates CoPhIR metaobject for every XML file it encounters.
 *
 * @author xbatko
 */
@SuppressWarnings("UseOfSystemOutOrSystemErr")
public class CophirMetaObjectDataBuilder implements FileProcessor, Closeable {
    /** Handler for parsing XML CoPhIR data */
    private final CophirXmlParser handler;
    /** XML parser to use */
    private final SAXParser parser;
    /** Stream to store whole meta object */
    private final OutputStream metaobjectStream;
    /** Map of descriptor names and their respective output streams where the descriptors are stored */
    private final Map<String, OutputStream> descriptorStreams;
    /** Flag whether to create {@link MetaObjectMap} instances (<tt>true</tt>) or {@link MetaObjectCophirKeywords} instances (<tt>false</tt>) */
    private final boolean createMetaObjectMap;

    /**
     * Creates a new XML file processor for CoPhIR data.
     * @param handler the XML parser handler
     * @param dir the directory where to create the files
     * @param descriptorNames the name of the descriptors that will be stored in separate files
     * @param outputFileNameSuffix suffix added to the file name ('metaobject' or descriptor name). If null, ".data" is used.
     * @param compress flag whether to compress the output files (by {@link GZIPOutputStream}
     * @param createMetaObjectMap flag whether to create {@link MetaObjectMap} instances (<tt>true</tt>) or {@link MetaObjectCophirKeywords} instances (<tt>false</tt>)
     * @throws ParserConfigurationException if there was an error initializing XML parser
     * @throws SAXException if there was an error initializing XML parser
     * @throws IOException if there was an error opening the output streams
     */
    protected CophirMetaObjectDataBuilder(CophirXmlParser handler, File dir, List<String> descriptorNames, String outputFileNameSuffix, boolean compress, boolean createMetaObjectMap) throws ParserConfigurationException, SAXException, IOException {
        final String suffix = ((outputFileNameSuffix == null) ? ".data" : outputFileNameSuffix);
        this.handler = handler;
        this.parser = SAXParserFactory.newInstance().newSAXParser();
        this.metaobjectStream = openStream(dir, "metaobject", suffix, compress);
        this.descriptorStreams = openStreamsForDescriptors(dir, descriptorNames, suffix, compress);
        this.createMetaObjectMap = createMetaObjectMap;
    }

    /**
     * Creates a new instance of {@link MetaObject} that encapsulates the given objects.
     * @param locator the locator for the meta object parsed from the XML
     * @param objects the encapsulated objects for the meta object parsed from the XML
     * @param textFields the textual fields parsed from the XML
     * @return a new instance of {@link MetaObject}
     */
    protected MetaObject createMetaObject(String locator, Map<String, LocalAbstractObject> objects, String[] textFields) {
        if (createMetaObjectMap)
            return new MetaObjectMap(locator, objects);
        else
            return new MetaObjectCophirKeywords(locator, objects, textFields);
    }

    /**
     * Creates a metaobject from the given XML input.
     * Note that the input stream is closed automatically.
     */
    public int processFile(String source, InputStream fileData) {
        try {
            handler.resetObjects();
            parser.parse(fileData, handler); // Note that this closes the input
            MetaObject metaObject = createMetaObject(handler.getLocatorURI(), handler.getObjects(), handler.getTextFields());
            for (Map.Entry<String, OutputStream> entry : descriptorStreams.entrySet()) {
                LocalAbstractObject descriptor = metaObject.getObject(entry.getKey());
                if (descriptor != null)
                    descriptor.write(entry.getValue());
            }
            metaObject.write(metaobjectStream);
            return 1;
        } catch (Exception e) {
            System.err.print("Error processing '");
            System.err.print(source);
            System.err.print("': ");
            System.err.println(e);
            return 0;
        }
    }

    /**
     * Opens an output stream.
     * @param dir the directory where the files are created
     * @param fileName the name of the file
     * @param compress flag whether the files should be compressed
     * @return an opened output stream
     * @throws IOException if there was a problem opening a new file in the given directory
     */
    private static OutputStream openStream(File dir, String fileName, String fileNameSuffix, boolean compress) throws IOException {
        OutputStream rtv = new BufferedOutputStream(new FileOutputStream(new File(dir, compress ? fileName + fileNameSuffix + ".gz" : fileName + fileNameSuffix)));
        return compress ? new GZIPOutputStream(rtv) : rtv;
    }

    /**
     * Opens an output stream for each of the given descriptor names.
     * @param dir the directory where the files are created
     * @param descriptorNames the names of the descriptors
     * @param suffix the created file names suffix
     * @param compress flag whether the files should be compressed
     * @return an array of opened streams
     * @throws IOException if there was a problem opening a new file in the given directory
     */
    private static Map<String, OutputStream> openStreamsForDescriptors(File dir, List<String> descriptorNames, String suffix, boolean compress) throws IOException {
        Map<String, OutputStream> rtv = new HashMap<String, OutputStream>(descriptorNames.size());
        for (String descriptorName : descriptorNames)
            rtv.put(descriptorName, openStream(dir, descriptorName, suffix, compress));
        return rtv;
    }

    public void close() throws IOException {
        for (OutputStream stream : descriptorStreams.values())
            stream.close();
        metaobjectStream.close();
    }

    /**
     * Prints usage for this tool.
     * If error is not <tt>null</tt>, the process is terminated with error code 1.
     * @param error the error to display
     */
    private static void usage(String error) {
        if (error != null)
            System.err.println(error);
        System.err.println("Usage: CophirMetaObjectDataBuilder [-suffix <fileNameSuffix>] [-z] [-m] [-kwdb <table> <jdbc>] [-stem <instance>] [-df <desc>] [--] <dir|xmlfile|tarfile> [<dir|xmlfile|tarfile> ...]");
        System.err.println("    -suffix ... string appended to the base file name ('metaobject' or descriptor name). Default value is '.data'");
        System.err.println("    -z    ... compress output data");
        System.err.println("    -meta ... create MetaObjectMap objects (i.e. with meta header) instead of MetaObjectCophirKeywords");
        System.err.println("    -nre  ... regular expression (or file name containing the names) to match the locators");
        System.err.println("    -kwdb ... table name and DB connection string for keyword processing, e.g.");
        System.err.println("                -db keywords jdbc:mysql://localhost/datasets?user=datasets&password=...&characterEncoding=utf-8");
        System.err.println("    -stem ... instantiation string for stemmer, e.g.");
        System.err.println("                -stem org.tartarus.snowball.MESSIFStemmer(english)");
        System.err.println("    -df   ... create separate file for the given descriptors, e.g.");
        System.err.println("                -df EdgeHistogramType -df ColorLayoutType -df ColorStructureType -df HomogeneousTextureType -df ScalableColorType -df Location -df KeyWordsType");
        System.err.println("    --    ... used to separate options from file name (if a filename start with hyphen)");
        System.err.println("New separate output files (<descriptor_name>.data) for individual CoPhIR descriptors as well as an aggregate file (metaobject.data) are created contains textual representation of all meta objects read in.");
        if (error != null)
            System.exit(1);
    }

    /**
     * Main method
     * @param args the command line arguments
     * @throws Exception if there was a problem
     */
    @SuppressWarnings("unchecked")
    public static void main(String[] args) throws Exception {
        int fileArg = -1;
        String fileNameSuffix = null;
        boolean compress = false;
        boolean createMetaObjectMap = false;
        Stemmer stemmer = null;
        Object fileNamePattern = null;
        IntStorageIndexed<String> wordIndex = null;
        List<String> descriptorFileList = new ArrayList<String>(7);

        // Parse arguments
        int i = 0;
        try {
            while (i < args.length) {
                if (args[i].startsWith("--") || !args[i].startsWith("-")) {       // File arguments from here
                    fileArg = i;
                    break;
                }
                // Parse an argument
                if (args[i].equals("-suffix")) {
                    fileNameSuffix = args[++i];
                } else if (args[i].equals("-z")) {
                    compress = true;
                } else if (args[i].equals("-meta")) {
                    createMetaObjectMap = true;
                } else if (args[i].equals("-nre")) {
                    fileNamePattern = FileSearchProcess.createFileNamePattern(args[i + 1]);
                    i++;
                } else if (args[i].equals("-kwdb")) {
                    wordIndex = new DatabaseStorage<String>(String.class,
                            args[i + 2], // JDBC connection string
                            null, args[i + 1], // Table name
                            "id", new String[] { "keyword" }, new DatabaseStorage.ColumnConvertor[] { DatabaseStorage.trivialColumnConvertor });
                    i += 2;
                } else if (args[i].equals("-stem")) {
                    stemmer = InstantiatorSignature.createInstanceWithStringArgs(args[i + 1], Stemmer.class, null);
                    i++;
                } else if (args[i].equals("-df")) {
                    descriptorFileList.add(args[i + 1]);
                    i++;
                } else {
                    usage("Unsupported argument has been passed: " + args[i]);
                }
                i++;
            }
        } catch (Exception e) {
            usage("Error parsing argument " + args[i] + ": " + e);
        }

        if (fileArg < 0)
            usage("No input file or directory has been passed.");

        // Processing
        CophirMetaObjectDataBuilder processor = new CophirMetaObjectDataBuilder(new CophirXmlParser(stemmer, wordIndex), null, descriptorFileList, fileNameSuffix, compress, createMetaObjectMap);
        try {
            FileSearchProcess.process(processor, fileNamePattern, i, args);
        } finally {
            processor.close();
        }
    }
}
