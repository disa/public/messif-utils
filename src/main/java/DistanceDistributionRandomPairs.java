/*
 *  This file is part of MESSIF library.
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import messif.objects.LocalAbstractObject;
import messif.objects.MetaObject;
import messif.objects.util.AbstractObjectList;
import messif.objects.util.AggregationFunction;
import messif.objects.util.StreamGenericAbstractObjectIterator;
import messif.objects.util.impl.AggregationFunctionEvaluator;
import messif.utility.Convert;
import messif.utility.reflection.InstantiatorSignature;

/**
 * Allows to compute a distance histogram of a given metric space. The metric space is represented by the data object class, the distances
 * are measured by taking random object pairs from a given data file.
 *
 * <p>
 * The whole data is read in and then random pairs are identified in the data.
 * </p>
 *
 * @author Michal Batko, Masaryk University, Brno, Czech Republic, batko@fi.muni.cz
 * @author Vlastislav Dohnal, Masaryk University, Brno, Czech Republic, dohnal@fi.muni.cz
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public class DistanceDistributionRandomPairs {

    /**
     * Prints the this class usage from the command line.
     *
     * @param stream a stream to which the usage info is written
     */
    private static void printUsage(PrintStream stream) {
        stream.println("Usage: DistanceDistributionRandomPairs <class> <data file> <number-of-pairs> <distance-interval> "
                + "[-aggfunc <agg_func_file>] [-printdists <dist_file>] [-a] [-z] [-d] [-n <number>] [-invoke <signature>] [-idim]");

        stream.println("    <number-of-pairs> if it is -1, all pairs are used systematically (actually upper diagonal is used only)");

        stream.println("       -a means 'additive' (i.e. the distribution, otherwise, the density is shown)");
        stream.println("       -z counts exact zero distance separately");
        stream.println("       -n <number> normalize the distance by <number>");
        stream.println("       -d distinct pairs: never calculate dist(x,x)");
        stream.println("       -idim computes intrinsic dimensionality over the random pairs (Navarro's algo)");
        stream.println("       -aggfunc <agg_func_file>  if used, this option reads the specified file interpreting the first line as input");
        stream.println("               string for AggregationFunctionEvaluator string. The object class must be derived from MetaObject");
        stream.println("       -printdists <dist_file>  if used, all the distances are printed into a separate file (use '-' for System.out) in format: ");
        stream.println("               (%d-%d): subdist1 ; subdist2; ... ; agg-dist         (if aggfunc used)");
        stream.println("               (%d-%d): distance                                    (if aggfunc NOT used)");
        stream.println("            where %d-%d means indexes (zero based) of objects whose distance has been computed.");
        stream.println("       -param[X] <param1> <param2> ... <paramX>");
        stream.println("             additional parameters for object constructor");
//        stream.println("       -invoke <signature>  if used, the method or constructor with the given signature is invoked");
    }

    /**
     * Prints a line of the distance histogram result.
     *
     * @param stream a stream to which the line is written
     * @param interval the distance
     * @param count
     */
    public static void printDistanceResult(PrintStream stream, double interval, Object count) {
        stream.printf((Locale) null, "%.2f: ", interval);
        stream.println(count);
    }

    /**
     * Main method that deals with the parameters and computes the histogram.
     *
     * @param args command line parameters (see usage)
     */
    public static void main(String[] args) {
        try {
            if (args.length < 4) {
                printUsage(System.err);
                return;
            }
            long numberPairs = Integer.valueOf(args[2]);
            double distanceInterval = Double.valueOf(args[3]);

            AggregationFunction function = null;
            boolean additive = false;
            PrintWriter distFileWriter = null;
            boolean separateZero = false;
            boolean distincive = false;
            double normalize = -1;
            Collection additionalParams = null;
            boolean doIDim = false;

            int argIndex = 4;
            while (argIndex < args.length) {
                if (args[argIndex].equalsIgnoreCase("-aggfunc")) {
                    //BufferedReader input = new BufferedReader(new FileReader(args[argIndex + 1]));
                    //function = new AggregationFunctionEvaluator(input.readLine());
                    //input.close();
                    function = new AggregationFunctionEvaluator(args[argIndex + 1]);
                    argIndex += 2;
                } else if (args[argIndex].equals("-a")) {
                    additive = true;
                    argIndex++;
                } else if (args[argIndex].equals("-z")) {
                    separateZero = true;
                    argIndex++;
                } else if (args[argIndex].equals("-d")) {
                    distincive = true;
                    argIndex++;
                } else if (args[argIndex].equals("-idim")) {
                    doIDim = true;
                    argIndex++;
                } else if (args[argIndex].equals("-n")) {
                    normalize = Double.valueOf(args[argIndex + 1]);
                    argIndex += 2;
                } else if (args[argIndex].equals("-printdists")) {
                    if (args[argIndex + 1].equals("-")) {
                        distFileWriter = new PrintWriter(System.out);
                    } else {
                        distFileWriter = new PrintWriter(args[argIndex + 1]);
                    }
                    argIndex += 2;
                } else if (args[argIndex].equals("-invoke")) {
                    InstantiatorSignature.createInstanceWithStringArgs(args[argIndex + 1], Object.class, null);
                    argIndex += 2;
                } else if (args[argIndex].startsWith("-param")) {
                    int nParams = Integer.parseInt(args[argIndex].substring(6));
                    additionalParams = new ArrayList<String>(nParams);
                    argIndex++;
                    for (int i = 0; i < nParams; i++) {
                        additionalParams.add(args[argIndex++]);
                    }
                } else {
                    throw new IllegalArgumentException("unknown option: " + args[argIndex]);
                }
            }

            // Open sample stream
            StreamGenericAbstractObjectIterator<LocalAbstractObject> iterator = new StreamGenericAbstractObjectIterator<LocalAbstractObject>(
                    Convert.getClassForName(args[0], LocalAbstractObject.class), // Class of objects in file
                    args[1], // File name
                    null, // variable values
                    additionalParams // constructor additional params
            );
            AbstractObjectList<LocalAbstractObject> list = new AbstractObjectList<LocalAbstractObject>(iterator);
            int nObjects = list.size();
            iterator.close();

            // Prepare the results holder - array of intervals
            List<AtomicLong> intervals = new ArrayList<>();

            // if the subdistances are to be printed out
            float[] subdistances = null;
            if ((function != null) && (distFileWriter != null)) {
                subdistances = new float[function.getParameterCount()];
            }
            int zeroDistCount = 0;

            // Test for complete data set test (all pairs)
            boolean doAllPairs = (numberPairs == -1);
            if (doAllPairs) {
                numberPairs = list.size() * (list.size() - 1) / 2;
            }

            Random random = new Random(System.currentTimeMillis());

            double[] distances = null;
            if (doIDim) {
                distances = new double[(int) numberPairs];
                if (numberPairs != (long) (int) numberPairs) {
                    System.err.printf("Warning: There are too many pairs, so Java array cannot store all!%n");
                }
            }

            // Begin computation
            long time = System.currentTimeMillis();
            if (doAllPairs) {
                System.err.printf("Computing over all pairs in data: %d objects, so %d pairs%n", list.size(), numberPairs);
                long pairsCounter = 0;
                for (int r1 = 0; r1 < list.size(); r1++) {
                    LocalAbstractObject object1 = list.get(r1);
                    for (int r2 = r1 + 1; r2 < list.size(); r2++) {
                        pairsCounter++;
                        LocalAbstractObject object2 = list.get(r2);

                        // Compute distance of two objects from the stream
                        double distance;
                        if (function != null) {
                            distance = function.getDistance((MetaObject) object1, (MetaObject) object2, subdistances);
                        } else {
                            distance = object1.getDistance(object2);
                        }
                        if (distance < 0) {
                            System.err.println("Object pair " + pairsCounter + " have returned distance: " + distance);
                            continue;
                        }

                        if (normalize > 0) {
                            distance = distance / normalize;
                        }

                        // Separate zero distance hack...
                        if (separateZero && distance == 0) {
                            zeroDistCount++;
                            pairsCounter--;      // get a new non-zero pair...
                            continue;
                        }

                        // Store the distance for iDim computation
                        if (doIDim) {
                            distances[(int) pairsCounter - 1] = distance;
                        }

                        // print the distances, if to be printed
                        if (distFileWriter != null) {
                            if (subdistances != null) {
                                StringBuilder distBuf = new StringBuilder();
                                for (int i = 0; i < subdistances.length; i++) {
                                    distBuf.append(subdistances[i]).append("; ");
                                }
                                distBuf.append(distance);
                                distFileWriter.printf("(%d-%d): %s\n", r1, r2, distBuf.toString());
                            } else {
                                distFileWriter.printf("(%d-%d): %f\n", r1, r2, distance);
                            }
                            distFileWriter.flush();
                        }

                        // Compute index of this distance
                        int distanceIndex = (int) Math.floor(distance / distanceInterval);

                        // Increase number of intervals if it is below this distance
                        while (intervals.size() <= distanceIndex) {
                            intervals.add(new AtomicLong(0));
                        }

                        // Insert the distance to the intervals
                        intervals.get(distanceIndex).incrementAndGet();

                        printProgress(pairsCounter, numberPairs);
                    }
                }

            } else {

                int r1, r2;
                for (long pairsCounter = 1; pairsCounter <= numberPairs; pairsCounter++) {
                    do {
                        r1 = random.nextInt(nObjects);
                        r2 = random.nextInt(nObjects);
                    } while (distincive && (r1 == r2));

                    LocalAbstractObject object1 = list.get(r1);
                    LocalAbstractObject object2 = list.get(r2);

                    // Compute distance of two objects from the stream
                    double distance;

                    if (function != null) {
                        distance = function.getDistance((MetaObject) object1, (MetaObject) object2, subdistances);
                    } else {
                        distance = object1.getDistance(object2);
                    }
                    if (distance < 0) {
                        System.err.println("Object pair " + pairsCounter + " have returned distance: " + distance);
                        continue;
                    }

                    if (normalize > 0) {
                        distance = distance / normalize;
                    }

                    // Separate zero distance hack...
                    if (separateZero && distance == 0) {
                        zeroDistCount++;
                        pairsCounter--;      // get a new non-zero pair...
                        continue;
                    }

                    // Store the distance for iDim computation
                    if (doIDim) {
                        distances[(int) pairsCounter - 1] = distance;
                    }

                    // print the distances, if to be printed
                    if (distFileWriter != null) {
                        if (subdistances != null) {
                            StringBuilder distBuf = new StringBuilder();
                            for (int i = 0; i < subdistances.length; i++) {
                                distBuf.append(subdistances[i]).append("; ");
                            }
                            distBuf.append(distance);
                            distFileWriter.printf("(%d-%d): %s\n", r1, r2, distBuf.toString());
                        } else {
                            distFileWriter.printf("(%d-%d): %f\n", r1, r2, distance);
                        }
                        distFileWriter.flush();
                    }

                    // Compute index of this distance
                    int distanceIndex = (int) Math.floor(distance / distanceInterval);

                    // Increase number of intervals if it is below this distance
                    while (intervals.size() <= distanceIndex) {
                        intervals.add(new AtomicLong(0));
                    }

                    // Insert the distance to the intervals
                    intervals.get(distanceIndex).incrementAndGet();

                    // Print percentage
                    printProgress(pairsCounter, numberPairs);
                }
            }
            time = System.currentTimeMillis() - time;
            System.err.println("Distance computations: " + numberPairs);
            System.err.println("Overall time: " + time / 1000 + " s");
            System.err.println("Distance comp. time less then: " + (((double) time) / ((double) numberPairs)) + " ms");
            System.err.flush();

            // now print results to stdout
            long sum = 0;
            int cnt = 0;
            if (separateZero) {
                printDistanceResult(System.out, 0, zeroDistCount);
                sum = zeroDistCount;
            }
            for (AtomicLong value : intervals) {
                if (additive) {
                    printDistanceResult(System.out, distanceInterval * (cnt++), sum += value.intValue());
                } else {
                    printDistanceResult(System.out, distanceInterval * (cnt++), value);
                }
            }

            if (doIDim) {
                double[] res = computeIntrinsicDimensionality(distances);
                System.out.printf((Locale) null, "IntrinsicDim: %.2f", res[0]);
                System.out.println();
                System.out.printf((Locale) null, "MeansDist: %.2f", res[1]);
                System.out.println();
                System.out.printf((Locale) null, "Variance: %.2f", res[2]);
                System.out.println();
            }

            System.out.flush();

            // close files
            if (distFileWriter != null) {
                distFileWriter.close();
            }

        } catch (Throwable e) {
            e.printStackTrace();
            printUsage(System.err);
        }
    }

    private static void printProgress(long pairsCounter, long numberPairs) {
        // Print percentage
        if (((10 * pairsCounter) / numberPairs) > ((10 * (pairsCounter - 1) / numberPairs))) {
            System.err.printf("Distances calculated: %d%%%n", ((100 * pairsCounter) / numberPairs));
        }
    }

    private static void getIndexes(int[] objIdxPair, int nObjects, Random random, boolean noSameIndex) {
        final int max = (noSameIndex) ? 1000 : 1;
        for (int i = 0; i < max; i++) {
            objIdxPair[0] = random.nextInt(nObjects);
            objIdxPair[1] = random.nextInt(nObjects);
            if (objIdxPair[0] != objIdxPair[1]) {
                break;
            }
        }
    }

    /**
     * Compute intrinsic dimensionality (rho) as: rho = mu^2 / (2 * sigma^2)
     *
     * TR/DCC-2000-1, Edgar Chavez, Gonzalo Navarro, Measuring the dimensionality of general metric spaces, June 2000.
     * ftp://ftp.dcc.uchile.cl/pub/users/gnavarro/metricmodel.ps.gz
     *
     * @param dists distances between objects in a data set
     * @return array of three float values (intrinsic dimensionality, mean value of all distances, variance of all distances)
     */
    public static double[] computeIntrinsicDimensionality(double[] dists) {
        double sum = 0f;
        for (double d : dists) {
            sum += d;
        }

        double mu = sum / (double) dists.length;

        double var = 0f;
        for (double d : dists) {
            var += Math.pow(d - mu, 2);
        }
        var /= (double) dists.length;

        if (var == 0f) {
            return new double[]{Float.MAX_VALUE, Float.MAX_VALUE, Float.MAX_VALUE};
        } else {
            return new double[]{Math.pow(mu, 2) / (2 * var), mu, var};
        }
    }

}
