/*
 *  This file is part of MESSIF library.
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.TreeMap;
import messif.objects.LocalAbstractObject;
import messif.objects.ObjectVector;
import messif.objects.impl.ObjectFloatVector;
import messif.objects.util.StreamGenericAbstractObjectIterator;
import messif.utility.Convert;

/**
 * Utility to check the metric postulates of a given data.
 * The metric space is represented by the data object class, the distances
 * are measured on a sample taken from a given data file.
 *
 * <p>
 * Note that the distances are computed in a stream fashion, so the data file
 * should be randomized first.
 * </p>
 *
 * @author Michal Batko, Masaryk University, Brno, Czech Republic, batko@fi.muni.cz
 * @author Vlastislav Dohnal, Masaryk University, Brno, Czech Republic, dohnal@fi.muni.cz
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public class MetricFunctionCheck {

    /**
     * Prints the this class usage from the command line.
     * @param stream a stream to which the usage info is written
     */
    private static void printUsage(PrintStream stream) {
        stream.println("Usage: MetricFunctionCheck [--noreflexivity] [--anglestats] [--dumpinvalid <filename>] <class> <data file> <number-of-triplets> [distance_equality_threshold]");
        stream.println(" The passed data file is read on the fly and just a triplet is held in mem at a time.");
        stream.println(" Individual triplets are read as consecutive objects from the file.");
        stream.println(" --anglestats: if used, the histogram of angular distances in the triplet 1-2-3 (angle at 2) is printed. This is valid for vector data only.");
        stream.println();
        stream.println(" CAVEAT: The order of optional arguments must be respected as printed above!");
    }

    /**
     * Main method that deals with the parameters and verifies the metric postulates.
     * @param args command line parameters (see usage)
     */
    public static void main(String[] args) {
        try {
            int argInd = 0;
            boolean disableReflexivity = false;
            PrintWriter dumpInvalidWriter = null;
            Map<Integer,Integer> angleStats = null;
            
            if (args[argInd].equals("--noreflexivity")) {
                disableReflexivity = true;
                argInd++;
            }
            if (args[argInd].equals("--anglestats")) {
                angleStats = new TreeMap<>();
                argInd++;
            }
            if (args[argInd].equals("--dumpinvalid")) {
                argInd++;
                dumpInvalidWriter = new PrintWriter(args[argInd++]);
            }
                
            // Open sample stream
            StreamGenericAbstractObjectIterator<LocalAbstractObject> iterator = new StreamGenericAbstractObjectIterator<LocalAbstractObject>(
                    Convert.getClassForName(args[argInd++], LocalAbstractObject.class), // Class of objects in file
                    args[argInd++] // File name
            );
            int numberTriplets = Integer.valueOf(args[argInd++]);
            double distanceEqualityThreshold = 0.000001;
            if (argInd < args.length)
                distanceEqualityThreshold = Math.abs(Double.parseDouble(args[argInd++]));

            // Begin computation
            long time = System.currentTimeMillis();
            int wrongReflexivity = 0;
            double wrongReflexivityDelta = 0;
            double wrongReflexivityDeltaMax = 0;
            int wrongSymmetry = 0;
            double wrongSymmetryDelta = 0;
            double wrongSymmetryMax = 0;
            int wrongTriangle = 0;
            double wrongTriangleDelta = 0;
            double wrongTriangleMax = 0;
            int counter = 1;
            try {
                for (; counter <= numberTriplets; counter++) {
                    LocalAbstractObject o1 = iterator.next();
                    LocalAbstractObject o2 = iterator.next();
                    LocalAbstractObject o3 = iterator.next();
                    float distO12 = o1.getDistance(o2);
                    float distO13 = o1.getDistance(o3);
                    float distO21 = o2.getDistance(o1);
                    float distO23 = o2.getDistance(o3);
                    float distO31 = o3.getDistance(o1);
                    float distO32 = o3.getDistance(o2);

                    if (!disableReflexivity) {
                        // Test reflexivity
                        float dist = o1.getDistance(o1);
                        if (dist > distanceEqualityThreshold) {
                            wrongReflexivityDelta += dist;
                            if (dist > wrongReflexivityDeltaMax)
                                wrongReflexivityDeltaMax = dist;
                            if (dumpInvalidWriter != null)
                                dumpInvalidWriter.println("reflexivity " + dist + " " + o1);
                            wrongReflexivity++;
                        }
                        dist = o2.getDistance(o2);
                        if (dist > distanceEqualityThreshold) {
                            wrongReflexivityDelta += dist;
                            if (dist > wrongReflexivityDeltaMax)
                                wrongReflexivityDeltaMax = dist;
                            if (dumpInvalidWriter != null)
                                dumpInvalidWriter.println("reflexivity " + dist + " " + o2);
                            wrongReflexivity++;
                        }
                        dist = o3.getDistance(o3);
                        if (dist > distanceEqualityThreshold) {
                            wrongReflexivityDelta += dist;
                            if (dist > wrongReflexivityDeltaMax)
                                wrongReflexivityDeltaMax = dist;
                            if (dumpInvalidWriter != null)
                                dumpInvalidWriter.println("reflexivity " + dist + " " + o3);
                            wrongReflexivity++;
                        }
                    }

                    // Test symmetry
                    if (distO12 != distO21 && Math.abs(distO12 - distO21) > distanceEqualityThreshold) {
                        double dist = Math.abs(distO12 - distO21);
                        wrongSymmetryDelta += dist;
                        if (dist > wrongSymmetryMax)
                            wrongSymmetryMax = dist;
                        if (dumpInvalidWriter != null)
                            dumpInvalidWriter.println("symmetry " + dist + " " + o1 + ", " + o2);
                        wrongSymmetry++;
                    }
                    if (distO13 != distO31 && Math.abs(distO13 - distO31) > distanceEqualityThreshold) {
                        double dist = Math.abs(distO13 - distO31);
                        wrongSymmetryDelta += dist;
                        if (dist > wrongSymmetryMax)
                            wrongSymmetryMax = dist;
                        if (dumpInvalidWriter != null)
                            dumpInvalidWriter.println("symmetry " + dist + " " + o1 + ", " + o3);
                        wrongSymmetry++;
                    }
                    if (distO23 != distO32 && Math.abs(distO23 - distO32) > distanceEqualityThreshold) {
                        double dist = Math.abs(distO23 - distO32);
                        wrongSymmetryDelta += dist;
                        if (dist > wrongSymmetryMax)
                            wrongSymmetryMax = dist;
                        if (dumpInvalidWriter != null)
                            dumpInvalidWriter.println("symmetry " + dist + " " + o2 + ", " + o3);
                        wrongSymmetry++;
                    }

                    // Test triangle inequality
                    if (distO12 + distO23 < distO13) {
                        double dist = distO13 - distO12 - distO23;
                        wrongTriangleDelta += dist;
                        if (dist > wrongTriangleMax)
                            wrongTriangleMax = dist;
                        if (dumpInvalidWriter != null)
                            dumpInvalidWriter.println("triangle " + dist + " " + o1 + ", " + o2 + ", " + o3);
                        wrongTriangle++;
                    }
                    if (distO23 + distO31 < distO21) {
                        double dist = distO21 - distO23 - distO31;
                        wrongTriangleDelta += dist;
                        if (dist > wrongTriangleMax)
                            wrongTriangleMax = dist;
                        if (dumpInvalidWriter != null)
                            dumpInvalidWriter.println("triangle " + dist + " " + o2 + ", " + o3 + ", " + o1);
                        wrongTriangle++;
                    }
                    if (distO31 + distO12 < distO32) {
                        double dist = distO32 - distO31 - distO12;
                        wrongTriangleDelta += dist;
                        if (dist > wrongTriangleMax)
                            wrongTriangleMax = dist;
                        wrongTriangle++;
                        if (dumpInvalidWriter != null)
                            dumpInvalidWriter.println("triangle " + dist + " " + o3 + ", " + o1 + ", " + o2);
                    }
                    
                    // Angle in the triplet
                    if (angleStats != null) {
                        if (!(o2 instanceof ObjectVector)) {
                            System.out.println("WARNING: Data objects are not vectors. Angles will not be computed!");
                            angleStats = null;
                        } else {
                            float[] v2 = getVector(o2); // origin where the angle is computed
                            float[] v1 = translate(v2, getVector(o1));
                            float[] v3 = translate(v2, getVector(o3));
                            double a = angle(v1, v3);
                            int ai = (int) Math.round(a);
                            int cnt = angleStats.getOrDefault(ai, 0);
                            angleStats.put(ai, cnt+1);
                        }
                    }
                }
            } catch (NoSuchElementException e) {
                // No more data in the file.
                System.out.println("WARNING: End of file reached. Triplets tested so far: " + (counter-1));
            }
            time = System.currentTimeMillis() - time;
            if (dumpInvalidWriter != null)
                dumpInvalidWriter.close();
            int totalDC = (disableReflexivity ? 6 : 9) * numberTriplets;
            System.out.println("Distance computations per test: " + 3 * numberTriplets);
            System.out.println("Overall time: " + time + " ms");
            System.out.println("Distance comp. time less then: "+ (((double)time) / ((double)totalDC)) + " ms");
            System.out.println("Equality threshold (for reflexivity and symmetry): " + distanceEqualityThreshold);
            if (!disableReflexivity)
                System.out.println("Reflexivity (valid, invalid, violation avg/max): " +
                        (numberTriplets * 3 - wrongReflexivity) + ", " +
                        wrongReflexivity + ", " +
                        (wrongReflexivityDelta / wrongReflexivity) + "/" +
                        wrongReflexivityDeltaMax
                );
            System.out.println("Symmetry (valid, invalid, violation avg/max): " +
                    (numberTriplets * 3 - wrongSymmetry) + ", " +
                    wrongSymmetry + ", " +
                    (wrongSymmetryDelta / wrongSymmetry) + "/" +
                    wrongSymmetryMax
            );
            System.out.println("Triangle inequality (valid, invalid, violation avg/max): " +
                    (numberTriplets * 3 - wrongTriangle) + ", " +
                    wrongTriangle + ", " +
                    (wrongTriangleDelta / wrongTriangle) + "/" +
                    wrongTriangleMax
            );
            
            // Print angle stats
            if (angleStats != null) {
                System.out.println("Histogram of angles for each triplet (1-2-3), angle at 2: (angle:freq)");
                for (Map.Entry<Integer, Integer> e : angleStats.entrySet()) {
                    System.out.println(e.getKey() + ": " + e.getValue());
                }
            }
            
        } catch (Throwable e) {
            e.printStackTrace();
            printUsage(System.err);
        }
    }

    private static float[] getVector(LocalAbstractObject o) {
        ObjectFloatVector v = (ObjectFloatVector)o;
        return v.getVectorData();
    }
    
    private static double norm(float[] v) {
        double sum = 0f;
        for (float f : v)
            sum += f*f;
        return Math.sqrt(sum);
    }
    
    private static double dotProduct(float[] v1, float[] v2) {
        double sum = 0f;
        for (int i = 0; i < v1.length; i++)
            sum += v1[i] * v2[i];
        return sum;
    }
    
    /** Modifies the passed vector "v" !!! */
    private static float[] translate(float[] origin, float[] v) {
        for (int i = 0; i < origin.length; i++)
            v[i] -= origin[i];
        return v;
    }
    
    /** @return angle in degrees (0-180) between the translated vectors (they both origin at &lt;0&gt; vector) */
    private static double angle(float[] v1translated, float[] v2translated) {
        return 180.0 * Math.acos(dotProduct(v1translated, v2translated) / norm(v1translated) / norm(v2translated)) / Math.PI;
    }
}
