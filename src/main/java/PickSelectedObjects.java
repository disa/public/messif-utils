
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.zip.GZIPOutputStream;
import messif.objects.util.AbstractObjectIterator;
import messif.objects.LocalAbstractObject;
import messif.objects.util.AbstractObjectList;
import messif.objects.util.StreamGenericAbstractObjectIterator;
import messif.utility.Convert;

/**
 *
 * @author xbatko
 */
public class PickSelectedObjects {

    protected static void printUsage() {
        // Wrong argument count
        System.err.println("Usage: PickSelectedObjects <objects' class> [-in <input object file>] [-out|-gzout <output object file>] "
                + "[-preserveorder] [-padding <length>] {-ids|-fileids|-pos} <locator id OR position (zero based) OR one file name> ...");
        System.err.println("       The objects are appended to the output file. If the output file does not exist, it is created.");
        System.err.println("       If the input file is not specified, System.in is used.");
        System.err.println("       If the output file is not specified, System.out is used.");
        System.err.println("       -gzout <file_name> -- produced gzipped output");
        System.err.println("       -preserveorder  -- preserves the ordering of IDs given by -ids|-fileids|-pos parameters. Otherwise "
                + "ordering of objects is based on the input file.");
        System.err.println("       -padding <length> -- pad IDs read from a file to the given length with zeros. Padding is also applied "
                + "to input object if the object's URI does not match.");
        System.err.println("       -fileids <file_name> -- specified a file name from which object IDs are read (one ID per line)");
    }

    private static boolean isOption(String arg) {
        if (!arg.startsWith("-"))
            return false;
        return (arg.equals("-in") || arg.equals("-out") || arg.equals("-gzout")
                || arg.equals("-preserveorder") || arg.equals("-padding")
                || arg.equals("-pos") || arg.equals("-ids") || arg.equals("-fileids"));
    }
    
    public static void main(String... args) throws Exception {
        try {
            boolean preserveOrderByIds = false;
            AbstractObjectIterator<LocalAbstractObject> input = null;
            // List of outputs and IDs
            List<OutputStream> outputStreams = new ArrayList<>();
            List<Set<String>> locatorURISets = new ArrayList<>();
            List<SortedSet<Integer>> positionSets = new ArrayList<>();
            
            // Objects class
            Class<LocalAbstractObject> objectsClass = Convert.getClassForName(args[0], LocalAbstractObject.class);

            // Parse arguments and open input and output files
            int argIndex = 1;
            while (argIndex < args.length) {
                // Object input file
                if (args[argIndex].equals("-in")) {
                    if (input != null) {
                        System.out.println("ERROR: Only one -in parameter can be given!");
                        System.exit(1);
                        return;
                    }
                    System.out.println("Input: " + args[argIndex + 1]);
                    input = new StreamGenericAbstractObjectIterator<LocalAbstractObject>(objectsClass, args[argIndex + 1]);
                    argIndex += 2;

                // Object output file
                } else if (args[argIndex].equals("-out") || args[argIndex].equals("-gzout")) {
                    System.out.println("Output: " + args[argIndex + 1]);
                    OutputStream output = new FileOutputStream(args[argIndex + 1], true);
                    if (args[argIndex].equals("-gzout"))
                        output = new GZIPOutputStream(output);
                    argIndex += 2;
                    outputStreams.add(output);

                } else if (args[argIndex].equals("-preserveorder")) {
                    System.out.println("Order of objects: same as given in a parameter '-ids|-fileids|-pos'");
                    preserveOrderByIds = true;
                    argIndex++;
                    
                } else if (args[argIndex].equals("-padding")) {
                    padding = Integer.parseInt(args[argIndex+1]);
                    System.out.println("IDs read from a file will be padded to length of " + padding);
                    argIndex += 2;

                // Get object by positions...
                } else if (args[argIndex].equals("-pos")) {
                    System.out.print("Reading absolute object positions from input...");
                    // Get all positions from cmd line and sort them
                    SortedSet<Integer> positions = new TreeSet<Integer>();
                    for (argIndex++; argIndex < args.length; argIndex++) {
                        if (args[argIndex].startsWith("-")) // next option detected...
                            break;
                        positions.add(Integer.parseInt(args[argIndex])+1);
                    }
                    positionSets.add(positions);


                // Get objects by their IDs
                } else if (args[argIndex].equals("-ids") || args[argIndex].equals("-fileids")) {
                    System.out.print("Reading objects from input and checking their IDs...");

                    Set<String> locatorURIs = new LinkedHashSet<String>();
                    if (args[argIndex].equals("-ids")) {
                        // Get all locatorURIs from cmd line
                        for (argIndex++; argIndex < args.length; argIndex++) {
                            if (isOption(args[argIndex])) // next option detected...
                                break;
                            locatorURIs.add(args[argIndex]);
                        }
                    } else {
                        // Get all locatorURIs from a file
                        try {
                            BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(args[++argIndex])));
                            String uri;
                            while ( (uri = in.readLine()) != null ) {
                                if (!uri.isEmpty()) {
                                    if (isPadding())
                                        uri = pad(uri);
                                    locatorURIs.add(uri);
                                }
                            }
                            in.close();
                        } catch (IOException ex) {
                            System.err.println("Failure during reading '-fileids' file: " + ex.getMessage());
                            System.err.println(ex.toString());
                        }
                        argIndex++; // skip the file name (as was processed)
                    }
                    locatorURISets.add(locatorURIs);

                } else {
                    throw new IllegalArgumentException("Unrecognized parameter at index " + argIndex + ": " + args[argIndex]);
                }
            }
            
            //****************************************
            // Process the specified action
            //****************************************

            // Use stdin if not given
            if (input == null) {
                System.out.println("Input: stdin");
                input = new StreamGenericAbstractObjectIterator<LocalAbstractObject>(objectsClass, new BufferedReader(new InputStreamReader(System.in)));
            }
            if (!preserveOrderByIds)
                System.out.println("Order of objects: same as in the input file given in '-input' parameter");

            if (positionSets.isEmpty() && locatorURISets.isEmpty())
                throw new IllegalArgumentException("Either -ids or -pos or -fileids must be specified.");
            if (!positionSets.isEmpty() && !locatorURISets.isEmpty())
                throw new IllegalArgumentException("The parameters -ids  and (-pos or -fileids) must not be combined!");

            if (outputStreams.isEmpty())
                System.out.println("Output: stdin");
            while (outputStreams.size() < Math.max(positionSets.size(), locatorURISets.size()))
                outputStreams.add(System.out);
            
            if (!positionSets.isEmpty()) {
                if (preserveOrderByIds)
                    throw new IllegalArgumentException("WARNING: Preserve order in IDs is not compatible with -pos option!");
                processByPositions(input, positionSets, outputStreams);
            }
            
            if (!locatorURISets.isEmpty()) {
                // Find the objects and write them out
                if (preserveOrderByIds) 
                    findObjectsByIDPreserveOrder(input, outputStreams, locatorURISets);
                else
                    findObjectsByID(input, outputStreams, locatorURISets);
                
                // There were additional objects that was not found
                locatorURISets.forEach((set) -> {
                    if (set.size() > 0) {
                        System.err.print("IDs not found: ");
                        System.err.println(set);
                    }
                });
            }
            
            for (OutputStream output : outputStreams) {
                if (output != null && output != System.out)
                    output.close();
            }
                
        } catch (IndexOutOfBoundsException e) {
            // Wrong argument count
            System.err.println(e.getMessage());
            System.err.println();
            printUsage();
        }
    }

    static int padding = -1;
    
    private static boolean isPadding() {
        return padding != -1;
    }

    private static String pad(String uri) {
        StringBuilder sb = new StringBuilder();
        for (int l = padding - uri.length(); l > 0; l--)
            sb.append('0');
        sb.append(uri);
        return sb.toString();
    }
    
    private static void findObjectsByID(AbstractObjectIterator<LocalAbstractObject> input, 
                                    List<OutputStream> outputStreams, 
                                    List<Set<String>> locatorURISets) {
        try {
            while (input.hasNext() && hasNextLocator(locatorURISets)) {
                LocalAbstractObject o = input.next();
                matchAndOutput(o, locatorURISets, outputStreams);
            }
        } catch (IOException ex) {
            System.err.println("Cannot write object to output: " + ex.getMessage());
            System.err.println(ex.toString());
        } catch (NoSuchElementException ex) {
            System.err.println("End of input: " + ex.getMessage());
        }
    }
    
    private static boolean hasNextLocator(List<Set<String>> locatorURISets) {
        for (Set<String> s : locatorURISets) {
            if (!s.isEmpty())
                return true;
        }
        return false;
    }

    private static boolean matchAndOutput(LocalAbstractObject o, 
                                    List<Set<String>> locatorURISets, 
                                    List<OutputStream> outputStreams) throws IOException {
        boolean matched = false;
        String uri = o.getLocatorURI();
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < locatorURISets.size(); j++) {
                final Set<String> ids = locatorURISets.get(j);
                if (ids.contains(uri)) {
                    o.write(outputStreams.get(j));
                    ids.remove(uri);
                    matched = true;
                }
            }
            // Check for padded ID
            if (isPadding())
                uri = pad(uri);
            else
                break;
        }
        return matched;
    }

    private static boolean matchAndOutput(LocalAbstractObject o, 
                                    String matchUri, 
                                    OutputStream output) throws IOException {
        String uri = o.getLocatorURI();
        for (int i = 0; i < 2; i++) {
            if (matchUri.equals(uri)) {
                o.write(output);
                return true;
            }
            // Check for padded ID
            if (isPadding())
                uri = pad(uri);
            else
                break;
        }
        return false;
    }
    
    private static void findObjectsByIDPreserveOrder(AbstractObjectIterator<LocalAbstractObject> input, 
                                    List<OutputStream> outputStreams, 
                                    List<Set<String>> locatorURISets) {
        try {
            // Read all data into memory
            AbstractObjectList<LocalAbstractObject> data = new AbstractObjectList<LocalAbstractObject>(input);
            // Go through all IDs to match
            for (int i = 0; i < locatorURISets.size(); i++) {
                final Set<String> ids = locatorURISets.get(i);
                final OutputStream stream = outputStreams.get(i);
                ids.forEach((id) -> {
                    // Go through all input objects
                    for (AbstractObjectIterator<LocalAbstractObject> itObjs = data.iterator(); itObjs.hasNext();) {
                        LocalAbstractObject o = itObjs.next();
                        try {
                            if (matchAndOutput(o, id, stream))
                                break;
                        } catch (IOException ex) {
                            System.err.println("Cannot write object to output: " + ex.getMessage());
                            System.err.println(ex.toString());
                        }
                    }                    
                });
            }
        } catch (NoSuchElementException ex) {
            System.err.println("End of input: " + ex.getMessage());
        }
    }

    private static void processByPositions(AbstractObjectIterator<LocalAbstractObject> input, List<SortedSet<Integer>> positionSets, 
                            List<OutputStream> outputStreams) throws IOException {
        List<PositionStreamPair> positions = new ArrayList<>();
        for (int i = 0; i < positionSets.size(); i++) {
            OutputStream stream = outputStreams.get(i);
            positionSets.get(i).forEach((pos) -> {
                positions.add(new PositionStreamPair(pos, stream));
            });
        }
        Collections.sort(positions);
        
        // Write the objects out
        int lastPos = 0;
        try {
            for (PositionStreamPair p : positions) {
                int seek = p.position - lastPos;
                lastPos += seek;
                input.getObjectByPosition(seek).write(p.stream);
            }
        } catch (NoSuchElementException e) {
            // There was additional objects that was not found
            System.err.print("Positions not found after the position: " + (lastPos + 1));
        }
    }
    
    private static class PositionStreamPair implements Comparable<PositionStreamPair> {
        private final int position;
        private final OutputStream stream;

        public PositionStreamPair(int position, OutputStream stream) {
            this.position = position;
            this.stream = stream;
        }

        @Override
        public int compareTo(PositionStreamPair o) {
            return Integer.compare(this.position, o.position);
        }
    }
}
