


import java.io.File;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import messif.algorithms.Algorithm;
import messif.objects.LocalAbstractObject;
import messif.objects.util.RankedAbstractObject;
import messif.objects.util.StreamGenericAbstractObjectIterator;
import messif.operations.AnswerType;
import messif.operations.query.ApproxKNNQueryOperation;
import messif.operations.query.KNNQueryOperation;
import messif.utility.Convert;

/**
 * This class takes an index of "positive examples" + a dataset and sorts the dataset according to the likeliness
 *  that the data objects belongs to the set of "positive examples". The output is a sorted list of "ranks" and
 *  locators of the objects.
 *
 * @author David Novak david.novak(at)fi.muni.cz
 */
public class ClassificatorApplication {

    /**
     * # of nearest objects to be tested by the "classification" algorithm.
     */
    public static final int K_PARAM = 30;

    /**
     * Number of objects to be accessed locally during kNN search.
     */
    public static final int OBJ_TO_ACCESS_COUNT = 1000;

    /**
     * Prints the program usage information to given output stream
     * @param out output stream
     */
    public static void usage(PrintStream out) {
        out.println("Usage: ClassificatorApplication <class> <index-file> <data-file> [-s] [-v] [-t] [<score-type-idx>]");
        out.println("    compare objects from <data-file> with index and print a list of" +
                    "    this data locators ordered according to 'distance from the index'.");
        out.println("  class           metric object class");
        out.println("  index-file      serialized similarity index - M-Index algorithm");
        out.println("  data-file       file with data objects to be tested");
        out.println("  -v              flag says that the 10 distances should be put to output as well");
        out.println("  -t              testing - do all possiblities for various 'k' and print results to seperate files");
        out.println("  -s              output in 'simple text' instead of default XML output");
        out.println("  score-type-idx  0='weighted sum of dists', 1='sum of dists' (default), 2='first dist', 3='power-weighted sum'");
    }

    protected static final int WEIGHTED_SUM_IDX = 0;

    protected static float getScoreByWeightedSum(KNNQueryOperation queryResult, int maxK) {
        float retVal = 0f;
        int weight = maxK;
        int i = 0;
        for (Iterator<RankedAbstractObject> it = queryResult.getAnswer(); it.hasNext() && i < maxK; i++) {
            retVal += (weight--) * it.next().getDistance();
        }

        return retVal;
    }

    protected static final int WEIGHTED_SUM_POWER_IDX = 3;

    protected static float getScoreByWeightedPowerSum(KNNQueryOperation queryResult, int maxK) {
        float retVal = 0f;
        float weight = 1f;

        int i = 0;
        for (Iterator<RankedAbstractObject> it = queryResult.getAnswer(); it.hasNext() && i < maxK; i++) {
            retVal += (weight) * it.next().getDistance();
            weight *= 0.7f;
        }

        return retVal;
    }

    protected static final int SUM_IDX = 1;

    protected static float getScoreBySum(KNNQueryOperation queryResult, int maxK) {
        float retVal = 0f;

        int i = 0;
        for (Iterator<RankedAbstractObject> it = queryResult.getAnswer(); it.hasNext() && i < maxK; i++) {
            retVal += it.next().getDistance();
        }

        return retVal;
    }


    protected static final int FIRST_DIST_IDX = 2;

    protected static float getScoreByFirstDistance(KNNQueryOperation queryResult) {
        return queryResult.getAnswer().next().getDistance();
    }

    //

    private static void printResults(ScoreLocatorPair[] resultArray, PrintStream stream, boolean simpleOutput) {
        // print simple the result
        if (simpleOutput) {
            int printed = 1;
            for (ScoreLocatorPair scoreLocatorPair : resultArray) {
                stream.print(printed++);
                stream.print(" ");
                stream.print(String.format("%.2f", scoreLocatorPair.score));
                stream.print(scoreLocatorPair.verboseOutput == null ? "" : scoreLocatorPair.verboseOutput);
                stream.print(": ");
                stream.println(scoreLocatorPair.locator);
            }
        } else { // XML output
            // generate the XML header
            stream.println("<?xml version=\"1.0\"?>\n");
            stream.println("<imagelist xmlns=\"http://www.w3schools.com\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" " +
                    "xsi:schemaLocation=\"http://www.w3schools.com imagelist.xsd\">");
            
            int printed = 1;
            for (ScoreLocatorPair scoreLocatorPair : resultArray) {
                stream.println("\t<image>");
                stream.println("\t\t<order>" + printed++ + "</order>");
                stream.println("\t\t<score>" + String.format("%.2f", scoreLocatorPair.score) + "</score>");
                stream.println("\t\t<id>" + scoreLocatorPair.locator + "</id>");
                stream.println("\t</image>");
            }

            stream.println("</imagelist>");
        }
    }


    /**
     * This simple class encapsulates pair (score,locator) naturally sorted according to
     *  score (if equal then by locator alphabetically).
     */
    protected static class ScoreLocatorPair implements Comparable<ScoreLocatorPair> {
        protected final float score;
        protected final String locator;
        protected final String verboseOutput;

        public ScoreLocatorPair(float score, String locator, String verboseOutput) {
            this.score = score;
            this.locator = locator;
            this.verboseOutput = verboseOutput;
        }

        public int compareTo(ScoreLocatorPair o) {
            int compare = Float.compare(score, o.score);
            if (compare == 0) {
                return locator.compareTo(o.locator);
            }
            return compare;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final ScoreLocatorPair other = (ScoreLocatorPair) obj;
            if (this.score != other.score) {
                return false;
            }
            if ((this.locator == null) ? (other.locator != null) : !this.locator.equals(other.locator)) {
                return false;
            }
            return true;
        }

        @Override
        public int hashCode() {
            int hash = 7;
            hash = 97 * hash + Float.floatToIntBits(this.score);
            hash = 97 * hash + (this.locator != null ? this.locator.hashCode() : 0);
            return hash;
        }

        @Override
        public String toString() {
            return score + ": " + locator;
        }
    }

    /**
     * 
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            if (args.length < 3) {
                usage(System.err);
                System.exit(1);
            }

            // deserialize the index
            Algorithm algorithm = Algorithm.restoreFromFile(args[1]);

            // read the objects in
            StreamGenericAbstractObjectIterator<LocalAbstractObject> iterator = new StreamGenericAbstractObjectIterator<LocalAbstractObject>(
                    Convert.getClassForName(args[0], LocalAbstractObject.class), // Class of objects in file
                    args[2] // File name
                    );

            // testing - do al possiblities for various "k" and print results to seperate files
            boolean test = false;
            
            // verbose output? (10 closest distances)
            boolean verboseOutput = false;

            // verbose output? (10 closest distances)
            boolean simpleOutput = false;

            // set the score calculation method
            int scoreType = SUM_IDX;

            int argIdx = 3;
            while (args.length > argIdx) {
                // testing - do all possibilities for various 
                if (args[argIdx].equals("-t")) {
                    test = true;
                    argIdx ++;
                    continue;
                }
                if (args[argIdx].equals("-v")) {
                    verboseOutput = true;
                    argIdx ++;
                    continue;
                }
                if (args[argIdx].equals("-s")) {
                    simpleOutput = true;
                    argIdx ++;
                    continue;
                }
                try {
                    scoreType = Integer.valueOf(args[argIdx]);
                    continue;
                } catch (NumberFormatException ignore) {
                    System.err.println("Wrong score calculation method index (keeping default): " + args[argIdx]);
                }
            }

            
            // output list of locators according to
            ArrayList<ScoreLocatorPair> scoreLocators = new ArrayList<ScoreLocatorPair>();
            
            // for "test" run
            int [] testKs = new int [] { 30 };//{ 3, 5, 7, 10, 15, 20};
            String [] testScoreTypesStrings = new String [] { "sum", "first-distance", "weighted-sum", "weighted-power-sum" };
            int [] testScoreTypes = new int [] { SUM_IDX, FIRST_DIST_IDX, WEIGHTED_SUM_IDX, WEIGHTED_SUM_POWER_IDX };
            Map<String,ArrayList<ScoreLocatorPair>> testResults = new HashMap<String, ArrayList<ScoreLocatorPair>>();
            if (test) {
                for (String testScoreType : testScoreTypesStrings) {
                    for (int testK : testKs) {
                        testResults.put(testScoreType + "-" + testK, new ArrayList<ScoreLocatorPair>());
                    }
                }
            }

            // current K can differ fromthe K_PARAM
            int [] ksToRun = new int [] { K_PARAM };
            int [] scoreTypesToRun = new int [] { scoreType };
            if (test) {
                ksToRun = testKs;
                scoreTypesToRun = testScoreTypes;
            }
            while (iterator.hasNext()) {
                KNNQueryOperation operation = new ApproxKNNQueryOperation(iterator.next(), K_PARAM, AnswerType.ORIGINAL_OBJECTS, OBJ_TO_ACCESS_COUNT,
                        ApproxKNNQueryOperation.LocalSearchType.ABS_OBJ_COUNT, -1.0f);
                algorithm.executeOperation(operation);

                for (int currentScoreType : scoreTypesToRun) {
                    for (int currentK : ksToRun) {

                        // create the verbose ouput (distances)
                        String distancesString = null;
                        if (verboseOutput) {
                            StringBuilder builder = new StringBuilder(" (");
                            int i = 0;
                            for (Iterator<RankedAbstractObject> answer = operation.getAnswer(); answer.hasNext() && i < currentK; i++ ) {
                                builder.append(String.format("%.2f", answer.next().getDistance())).append(", ");
                            }
                            builder.delete(builder.length() - 2, builder.length());
                            builder.append(")");
                            distancesString = builder.toString();
                        }

                        if (operation.getAnswer().next().getDistance() == 0f) {
                            // ignore those objects that are in the index
                            if (test) {
                                System.err.println("ignoring testing object that is in the index: " + operation.getQueryObject().getLocatorURI());
                            } else { // if the object is in the database, set its score to 0 always
                                scoreLocators.add(new ScoreLocatorPair(0f, operation.getQueryObject().getLocatorURI(), distancesString));
                            }
                            continue;
                        }

                        ScoreLocatorPair scoreLocatorPair;
                        String currentTestString = null;
                        switch (currentScoreType) {
                            case WEIGHTED_SUM_IDX:
                                scoreLocatorPair = new ScoreLocatorPair(getScoreByWeightedSum(operation, currentK), operation.getQueryObject().getLocatorURI(), distancesString);
                                if (test) { currentTestString = "weighted-sum"; }
                                break;
                            case FIRST_DIST_IDX:
                                scoreLocatorPair = new ScoreLocatorPair(getScoreByFirstDistance(operation), operation.getQueryObject().getLocatorURI(), distancesString);
                                if (test) { currentTestString = "first-distance"; }
                                break;
                            case WEIGHTED_SUM_POWER_IDX:
                                scoreLocatorPair = new ScoreLocatorPair(getScoreByWeightedPowerSum(operation, currentK), operation.getQueryObject().getLocatorURI(), distancesString);
                                if (test) { currentTestString = "weighted-power-sum"; }
                                break;
                            default:
                                scoreLocatorPair = new ScoreLocatorPair(getScoreBySum(operation, currentK), operation.getQueryObject().getLocatorURI(), distancesString);
                                if (test) { currentTestString = "sum"; }
                        }
                        
                        if (test) {
                            testResults.get(currentTestString + "-" + currentK).add(scoreLocatorPair);
                        } else {
                            scoreLocators.add(scoreLocatorPair);
                        }
                    }
                }
                
            }

            // iterate
            if (test) {
                File outputDir = new File("results");
                if (! outputDir.exists()) {
                    outputDir.mkdir();
                }
                if (! outputDir.isDirectory()) {
                    outputDir = new File(System.getProperty("user.dir"));
                }
                for (Map.Entry<String, ArrayList<ScoreLocatorPair>> pair : testResults.entrySet()) {
                    // ouput file
                    File outputFile = new File(outputDir, pair.getKey()+".txt");
                    PrintStream outputStream = new PrintStream(outputFile);
                    
                    ScoreLocatorPair[] resultArray = pair.getValue().toArray(new ScoreLocatorPair [1]);
                    Arrays.sort(resultArray);

                    printResults(resultArray, outputStream, simpleOutput);

                    outputStream.close();
                }

            } else {
                // sort the result according to the score
                ScoreLocatorPair[] resultArray = scoreLocators.toArray(new ScoreLocatorPair [1]);
                Arrays.sort(resultArray);

                printResults(resultArray, System.out, simpleOutput);
            }

        } catch (Throwable e) {
            System.err.println(e.toString());
        }

    }


}
