package messif.utility;

import com.ice.tar.TarEntry;
import com.ice.tar.TarInputStream;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.HashSet;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;
import java.util.zip.GZIPInputStream;

/**
 * Provides utilities for file searching.
 * All files in a given directory or TAR file can be processed by
 * a generic {@link FileProcessor} using the static methods in this class.
 * 
 * @author xbatko
 */
public abstract class FileSearchProcess {

    /**
     * Creates a file name pattern either as a collection of strings read from a file
     * or as a compiled regular expression.
     * @param value the name of the file or regular expression
     * @return a file name pattern
     * @throws IOException if there was a problem reading the pattern file
     * @throws PatternSyntaxException if the value is not a valid regular exception 
     */
    public static Object createFileNamePattern(String value) throws IOException, PatternSyntaxException {
        if (value == null || value.isEmpty())
            return null;

        try {
            BufferedReader reader = new BufferedReader(new FileReader(value));
            Collection<String> pattern = new HashSet<String>();
            String line = reader.readLine();
            while (line != null) {
                pattern.add(line);
                line = reader.readLine();
            }
            reader.close();
            return pattern;
        } catch (FileNotFoundException ignore) {
            return Pattern.compile(value);
        }
    }

    /**
     * Returns whether the given {@code fileName} matches the given {@code fileNamePattern}.
     * The pattern can be a {@link Collection} of names, a compiled {@link Pattern},
     * or a regular expression string.
     * @param fileName the file name to match
     * @param fileNamePattern the pattern to match the file name to; if <tt>null</tt>, all file names are matching
     * @return <tt>true</tt> if the file name matches or the fileNamePattern is <tt>null</tt>
     */
    private static boolean fileNameMatches(String fileName, Object fileNamePattern) {
        if (fileNamePattern == null)
            return true;
        if (fileNamePattern instanceof Collection) {
            return ((Collection<?>)fileNamePattern).contains(fileName);
        } else if (fileNamePattern instanceof Pattern) {
            return ((Pattern)fileNamePattern).matcher(fileName).matches();
        } else {
            return fileName.matches(fileNamePattern.toString());
        }
    }

    /**
     * Process the given file.
     * Note that if the given file name does not match the given {@code fileNamePattern},
     * no processor is called and method returns 0.
     * 
     * @param processor the processor called for the file
     * @param file the file to process
     * @param fileNamePattern the regular expression to match the file name
     * @return either 1 if the file was processed, or 0 if the file name does not match the given {@code fileNamePattern}
     * @throws IOException if there was an error reading the file
     * @throws FileProcessorException if there was an error while processing the file
     */
    public static int processFile(FileProcessor processor, File file, Object fileNamePattern) throws IOException, FileProcessorException {
        if (fileNameMatches(file.getName(), fileNamePattern)) {
            processor.processFile(file.getPath(), new BufferedInputStream(new FileInputStream(file)));
            return 1;
        } else {
            return 0;
        }
    }

    /**
     * Process all files found (recursively) in the given directory.
     * 
     * @param processor the processor called for each file
     * @param dir the directory to start the search from
     * @param fileNamePattern the regular expression to match the searched files
     * @return number of processed files
     * @throws IOException if there was an error reading a file
     * @throws FileProcessorException if there were errors while processing the files
     */
    public static int processDir(FileProcessor processor, File dir, Object fileNamePattern) throws IOException, FileProcessorException {
        int rtv = 0;
        for (File file : dir.listFiles()) {
            if (file.isDirectory()) {
                rtv += processDir(processor, file, fileNamePattern);
            } else {
                rtv += processFile(processor, file, fileNamePattern);
            }
        }

        return rtv;
    }

    /**
     * Process all files in the given TAR file.
     * 
     * @param processor the processor called for each file
     * @param tarFile the TAR file to search
     * @param fileNamePattern the regular expression to match the file names in the TAR
     * @return number of processed files
     * @throws IOException if there was an error reading the TAR file
     * @throws FileProcessorException if there were errors while processing the files inside the TAR
     */
    public static int processTAR(FileProcessor processor, File tarFile, Object fileNamePattern) throws IOException, FileProcessorException {
        // Open tar file (with decompression if needed)
        InputStream stream = new BufferedInputStream(new FileInputStream(tarFile));
        if (tarFile.getName().toLowerCase().endsWith("gz"))
            stream = new GZIPInputStream(stream);
        TarInputStream tar = new TarInputStream(stream);
        int rtv = 0;
        try {
            // Read all entries in the tar file
            for (TarEntry entry = tar.getNextEntry(); entry != null; entry = tar.getNextEntry()) {
                // Ignore entries that does not match the file name pattern
                if (!fileNameMatches(entry.getName(), fileNamePattern) || entry.isDirectory())
                    continue;

                // Write metaobject to descriptor files
                processor.processFile(entry.getName(), new BufferedInputStream(tar) {
                    // This is necessary since SAXParser calls close of input stream after processing :-((
                    @Override
                    public void close() throws IOException {
                    }
                });
                rtv++;
            }
        } finally {
            tar.close();
        }
        return rtv;
    }

    /**
     * Process all files in the given directories or TAR files specified in {@code args}.
     * @param processor the processor called for each file
     * @param fileNamePattern the regular expression to match the file names in the directory or TAR
     * @param startArg index of the first argument that is a directory or TAR file
     * @param args the directory or TAR file paths
     * @return number of processed files
     * @throws IOException if there was an error reading a file
     * @throws FileProcessorException if there were errors while processing the files
     */
    public static int process(FileProcessor processor, Object fileNamePattern, int startArg, String... args) throws IOException, FileProcessorException {
        int rtv = 0;
        FileProcessorException exception = null;
        for (; startArg < args.length; startArg++) {
            File argFile = new File(args[startArg]);
            try {
                if (argFile.isDirectory())
                    rtv += processDir(processor, argFile, fileNamePattern);
                else if (args[startArg].matches(".*\\.(tar|tar\\.gz|tgz)$"))
                    rtv += processTAR(processor, argFile, fileNamePattern);
                else if (processFile(processor, argFile, fileNamePattern) == 1)
                    rtv += 1;
                else
                    exception = FileProcessorException.addException(exception, argFile.getName(), new IllegalArgumentException("Unknown file"));
            } catch (FileProcessorException e) {
                exception = FileProcessorException.addException(exception, argFile.getName(), e);
            }
        }
        if (exception != null)
            throw exception;
        return rtv;
    }

    /**
     * Wrap the given {@link FileProcessor} so that errors are reported on
     * {@link System#err} immediately. The wrapper also shows information
     * about the number of files processed so far after every {@code counterReporting}
     * files are processed.
     * 
     * @param processor the file processor to wrap
     * @param counterReporting the number of processed files after which the information is displayed
     * @return the wrapped processor
     */
    public static FileProcessor reportingProcessor(final FileProcessor processor, final int counterReporting) {
        return new FileProcessor() {
            private int counter;
            @Override
            @SuppressWarnings("UseOfSystemOutOrSystemErr")
            public int processFile(String source, InputStream fileData) {
                try {
                    int processedItems = processor.processFile(source, fileData);
                    counter++;
                    if (counterReporting > 0 && ((counter - 1) % counterReporting == counterReporting - 1))
                        System.out.println("Processed " + counter + " files");
                    return processedItems;
                } catch (Exception e) {
                    System.err.print("Error processing '");
                    System.err.print(source);
                    System.err.print("': ");
                    System.err.println(e);
                    return 0;
                }                
            }
        };
    }
}
