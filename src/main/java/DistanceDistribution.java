/*
 *  This file is part of MESSIF library.
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.atomic.AtomicInteger;
import messif.objects.LocalAbstractObject;
import messif.objects.MetaObject;
import messif.objects.util.AggregationFunction;
import messif.objects.util.StreamGenericAbstractObjectIterator;
import messif.objects.util.impl.AggregationFunctionEvaluator;
import messif.utility.Convert;
import messif.utility.reflection.InstantiatorSignature;

/**
 * Allows to compute a distance histogram of a given metric space.
 * The metric space is represented by the data object class, the distances
 * are measured on a sample taken from a given data file.
 *
 * <p>
 * Note that the distances are computed in a stream fashion, so the data file
 * should be randomized first.
 * </p>
 *
 * @author Michal Batko, Masaryk University, Brno, Czech Republic, batko@fi.muni.cz
 * @author Vlastislav Dohnal, Masaryk University, Brno, Czech Republic, dohnal@fi.muni.cz
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public class DistanceDistribution {
    /*
     * TODO:
     *   merge with FullDistanceDistribution
     *   rewrite as real object where parameters are passed to constructor
     */

    /**
     * Prints the this class usage from the command line.
     * @param stream a stream to which the usage info is written
     */
    private static void printUsage(PrintStream stream) {
        stream.println("Usage: DistanceDistribution <class> <data file> <number-of-pairs> <distance-interval> [-aggfunc <agg_func_file>] [-printdists <dist_file>] [-a] [-z] [-invoke <signature>]");
        stream.println("       -a means 'additive' (i.e. the distribution, otherwise, the density is shown)");
        stream.println("       -z counts exact zero distance separately");
        stream.println("       -aggfunc <agg_func_file>  if used, this option reads the specified file interpreting the first line as input");
        stream.println("               string for AggregationFunctionEvaluator string. The object class must be derived from MetaObject");
        stream.println("       -printdists <dist_file>  if used, all the distances are printed into a separate file (use '-' for System.out) in format: ");
        stream.println("               (%d-%d): subdist1 ; subdist2; ... ; agg-dist         (if aggfunc used)");
        stream.println("               (%d-%d): distance                                    (if aggfunc NOT used)");
        stream.println("            where %d-%d means indexes (zero based) of objects whose distance has been computed.");
        stream.println("       -invoke <signature>  if used, the method or constructor with the given signature is invoked");
    }

    /**
     * Prints a line of the distance histogram result.
     * @param stream a stream to which the line is written
     * @param interval the distance
     * @param count
     */
    public static void printDistanceResult(PrintStream stream, double interval, Object count) {
        stream.printf((Locale)null, "%.2f: ", interval);
        stream.println(count);
    }

    /**
     * Main method that deals with the parameters and computes the histogram.
     * @param args command line parameters (see usage)
     */
    @SuppressWarnings({"UseOfSystemOutOrSystemErr", "CallToThreadDumpStack"})
    public static void main(String[] args) {
        try {
            int numberPairs = Integer.valueOf(args[2]);
            double distanceInterval = Double.valueOf(args[3]);
            
            AggregationFunction function = null;
            boolean additive = false;
            PrintWriter distFileWriter = null;
            boolean separateZero = false;

            int argIndex = 4;
            while (argIndex < args.length) {
                if (args[argIndex].equalsIgnoreCase("-aggfunc")) {
                    BufferedReader input = new BufferedReader(new FileReader(args[argIndex + 1]));
                    function = new AggregationFunctionEvaluator(input.readLine());
                    input.close();
                    argIndex += 2;
                } else if (args[argIndex].equals("-a")) {
                    additive = true;
                    argIndex ++;
                } else if (args[argIndex].equals("-z")) {
                    separateZero = true;
                    argIndex ++;
                } else if (args[argIndex].equals("-printdists")) {
                    if (args[argIndex+1].equals("-"))
                        distFileWriter = new PrintWriter(System.out);
                    else
                        distFileWriter = new PrintWriter(args[argIndex + 1]);
                    argIndex += 2;
                } else if (args[argIndex].equals("-invoke")) {
                    InstantiatorSignature.createInstanceWithStringArgs(args[argIndex + 1], Object.class, null);
                    argIndex += 2;
                } else {
                    throw new IllegalArgumentException("unknown option: "+args[argIndex]);
                }
            }
            
            // Open sample stream
            StreamGenericAbstractObjectIterator<LocalAbstractObject> iterator = new StreamGenericAbstractObjectIterator<LocalAbstractObject>(
                    Convert.getClassForName(args[0], LocalAbstractObject.class), // Class of objects in file
                    args[1] // File name
            );
            
            // Prepare the results holder - array of intervals
            List<AtomicInteger> intervals = new ArrayList<AtomicInteger>();

            // if the subdistances are to be printed out
            float [] subdistances = null;
            if ((function != null) && (distFileWriter != null)) {
                subdistances = new float [function.getParameterCount()];
            }
            int zeroDistCount = 0;

            // Begin computation
            long time = System.currentTimeMillis();
            long pureDCTime = 0;
            LocalAbstractObject lastObject = iterator.next();
            for (int pairsCounter = 1; pairsCounter <= numberPairs; pairsCounter++) {
                // Compute distance of two objects from the stream
                double distance;
                LocalAbstractObject previousObject = lastObject;
                try {
                    LocalAbstractObject newObject = iterator.next();
                    long timeStamp = System.currentTimeMillis();
                    if (function != null) {
                        distance = function.getDistance((MetaObject) lastObject, (MetaObject) (lastObject = newObject), subdistances);
                    } else {
                        distance = lastObject.getDistance(lastObject = newObject);
                    }
                    pureDCTime += (System.currentTimeMillis() - timeStamp);
                } catch (RuntimeException e) {
                    System.err.println("Error when processing pair #" + pairsCounter + ":");
                    System.err.println(previousObject);
                    System.err.println(lastObject);
                    e.printStackTrace();
                    return;
                }
                if (distance < 0) {
                    System.err.println("Object pair " + pairsCounter + " have returned distance: " + distance);
                    continue;
                } 
                
                // Separate zero distance hack...
                if (separateZero && distance == 0) {
                    zeroDistCount++;
                    continue;
                }


                // print the distances, if to be printed
                if (distFileWriter != null) {
                    if (subdistances != null) {
                        StringBuilder distBuf = new StringBuilder();
                        for (int i = 0; i < subdistances.length; i++) {
                            distBuf.append(subdistances[i]).append("; ");
                        }
                        distBuf.append(distance);
                        distFileWriter.printf("(%d-%d): %s\n", pairsCounter-1, pairsCounter, distBuf.toString());
                    } else {
                        distFileWriter.printf("(%d-%d): %f\n", pairsCounter-1, pairsCounter, distance);
                    }
                    distFileWriter.flush();
                }

                // Compute index of this distance
                int distanceIndex = (int)Math.floor(distance/distanceInterval);

                // Increase number of intervals if it is below this distance
                while (intervals.size() <= distanceIndex)
                    intervals.add(new AtomicInteger(0));

                // Insert the distance to the intervals
                intervals.get(distanceIndex).incrementAndGet();

                // Print percentage
                if (((10 * pairsCounter) / numberPairs) > ((10 * (pairsCounter - 1) / numberPairs)))
                    System.err.printf("Distances calculated: %d%%%n", ((100 * pairsCounter) / numberPairs));
            }
            time = System.currentTimeMillis() - time;
            System.err.println("Distance computations: " + numberPairs);
            System.err.println("Overall time: " + time/1000 + " s");
            System.err.println("Distance comp. time less then: "+ (((double)time) / ((double)numberPairs)) + " ms");
            System.err.println("Distance comp. pure time: "+ (((double)pureDCTime) / ((double)numberPairs)) + " ms");
            System.err.flush();
            
            // close files
            iterator.close();

            // now print results to stdout
            int sum = 0;
            int cnt = 0;
            if (separateZero) {
                printDistanceResult(System.out, 0, zeroDistCount);
                sum = zeroDistCount;
            }
            for (AtomicInteger value : intervals) {
                if (additive)
                    printDistanceResult(System.out, distanceInterval*(cnt++), sum += value.intValue());
                else
                    printDistanceResult(System.out, distanceInterval*(cnt++), value);
            }
            System.out.flush();
            if (distFileWriter != null) {
                distFileWriter.close();
            }
        } catch (Throwable e) {
            e.printStackTrace();
            printUsage(System.err);
        }
    }
}
