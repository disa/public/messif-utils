package messif.utility;

import java.io.IOException;
import java.util.Collections;
import messif.algorithms.Algorithm;
import messif.objects.LocalAbstractObject;
import messif.objects.NoDataObject;
import messif.operations.RankingQueryOperation;
import messif.operations.query.KNNQueryOperation;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Simple {@link Algorithm} wrapper for the MESSIF-v3 REST API.
 * @author Michal Batko, Masaryk University, Brno, Czech Republic, batko@fi.muni.cz
 */
public class MessifV3JsonAlgorithm extends Algorithm {
    /** Class id for serialization */
    private static final long serialVersionUID = 1L;
    /** Host (IP address) where the service is running */
    private final String host;
    /** TCP port on which the service is running */
    private final int port;
    /** Name of the service to execute (including all necessary parameters)*/
    private final String service;

    /**
     * Creates a new REST service wrapper algorithm.
     * @param host the host (IP address) where the service is running
     * @param port the TCP port on which the service is running
     * @param service the name of the service to execute (including all necessary parameters)
     */
    @AlgorithmConstructor(description = "Wrapper of MESSIF-v3 algorithm using a REST service", arguments = {"host", "port", "service URL"})
    public MessifV3JsonAlgorithm(String host, int port, String service) {
        super("HttpService on " + host + ":" + port);
        this.host = host;
        this.port = port;
        this.service = service;
    }

    /**
     * Converts a {@link LocalAbstractObject query object} to MESSIF-v3 JSON representation.
     * Note that only the locator is used, the data are ignored.
     * @param queryObject any query object, but note that the data will be ignored!
     * @return JSON representation of the query object
     */
    protected JSONObject toQueryObjectJson(LocalAbstractObject queryObject) {
        JSONObject ret = new JSONObject();
        String locator = queryObject.getLocatorURI();
        if (locator.contains("://"))
            ret.put("_url", locator);
        else
            ret.put("_id", locator);
        return ret;
    }

    /**
     * Fills the answer of the given operation with the objects from the MESSIF-v3 JSON response.
     * @param operation the operation the answer of which to fill
     * @param response the MESSIF-v3 JSON response to parse
     */
    protected void fillOperationAnswer(RankingQueryOperation operation, JSONObject response) {
        JSONArray answerObjects = response.getJSONArray("answer_records");
        JSONArray answerDistances = response.getJSONArray("answer_distances");
        int answerCount = response.getInt("answer_count");
        for (int i = 0; i < answerCount; i++) {
            operation.addToAnswer(new NoDataObject(answerObjects.getJSONObject(i).getString("_id")), (float)answerDistances.getDouble(i), null);
        }
        operation.endOperation();
    }

    /**
     * Executes KNN query operation via the REST service.
     * @param operation the operation to execute
     * @throws IOException if there was a problem working with the REST service
     */
    public void executeSearch(KNNQueryOperation operation) throws IOException {
        JSONObject request = new JSONObject().put("query_record", toQueryObjectJson(operation.getQueryObject())).put("k", operation.getK());
        JSONObject response = new JSONObject(RestServiceAlgorithm.processHttpRequest(host, port, service, null, request));
        try {
            JSONObject status = response.getJSONObject("status");
            if (status.getInt("code") != 200)
                throw new IOException(status.getString("error description"));
        } catch (JSONException ignore) {
            // status code was not found, ignore and continue
        }
        fillOperationAnswer(operation, response);
    }
}
