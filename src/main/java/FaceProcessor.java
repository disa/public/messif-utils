import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import javax.imageio.ImageIO;
import messif.algorithms.Algorithm;
import messif.algorithms.AlgorithmMethodException;
import messif.objects.LocalAbstractObject;
import messif.objects.extraction.ExtractorDataSource;
import messif.objects.extraction.ExtractorException;
import messif.objects.extraction.MultiExtractor;
import messif.objects.impl.ObjectFaceLuxandDescriptor;
import messif.objects.keys.FaceKey;
import messif.objects.util.RankedAbstractObject;
import messif.objects.util.StreamGenericAbstractObjectIterator;
import messif.operations.query.RangeQueryOperation;
import messif.utility.Convert;

/**
 * Various utilities for processing faces.
 * 
 * @author xbatko
 * @see FaceKey
 */
public abstract class FaceProcessor {

    /**
     * Method for searching for similar faces in the given algorithm.
     * The query objects are first extracted from the given file using the extractor.
     * Then the {@link RangeQueryOperation} is used on the {@code algorithm}
     * to retrieve the most similar object within the given {@code threshold}.
     * The object's locator is then used for labeling the face in the query image.
     * @param queryImage the image to use
     * @param extractor the extractor used to retrieve the faces from the query image
     * @param algorithm the algorithm used to search for similar objects
     * @param threshold the distance radius used for the {@link RangeQueryOperation}
     *          executed on the {@code algorithm} in order to retrieve the similar objects
     * @param outlineSize the size of the face outline to draw (no outline is drawn if zero)
     * @param fontSize the size of the font used to draw the labels
     * @param unknownLabel the label used when there was no similar face found by the algorithm
     * @return a map of the query object and the closest object found
     * @throws AlgorithmMethodException if there was an error executing the {@link RangeQueryOperation}
     * @throws NoSuchMethodException if the algorithm does not support {@link RangeQueryOperation}
     * @throws IOException if there was a problem reading the query image
     * @throws ExtractorException if there was a problem extracting the faces
     */
    public static BufferedImage drawFaceLocatorsBySimilarFaces(File queryImage, MultiExtractor<? extends LocalAbstractObject> extractor, Algorithm algorithm, float threshold, int outlineSize, int fontSize, String unknownLabel) throws AlgorithmMethodException, NoSuchMethodException, IOException, ExtractorException {
        // Load the image
        BufferedImage image = ImageIO.read(queryImage);
        Graphics2D g = image.createGraphics();
        if (fontSize > 0)
            g.setFont(g.getFont().deriveFont(Font.BOLD, fontSize));

        // Call the external extraction and convert it to objects
        ExtractorDataSource dataSource = new ExtractorDataSource(queryImage);
        Iterator<? extends LocalAbstractObject> extractedFaces = extractor.extract(dataSource);

        // Extract each face, search for similar ones in the given algorithm and draw it
        while (extractedFaces.hasNext()) {
            LocalAbstractObject face = extractedFaces.next();
            FaceKey faceKey = (FaceKey)face.getObjectKey();
            if (outlineSize > 0)
                faceKey.drawFaceContour(g, outlineSize);
            Iterator<? extends RankedAbstractObject> similarFaces = algorithm.getQueryAnswer(new RangeQueryOperation(face, threshold, 1));
            if (similarFaces.hasNext())
                faceKey.drawFaceLabel(g, similarFaces.next().getObject().getLocatorURI());
            else if (unknownLabel != null)
                faceKey.drawFaceLabel(g, unknownLabel);
        }

        dataSource.close();
        if (extractedFaces instanceof Closeable)
            ((Closeable)extractedFaces).close();

        return image;
    }

    /**
     * Method for searching for similar objects in the given algorithm.
     * Note that {@link RangeQueryOperation} is used with the given {@code threshold}
     * and the first object returned in the answer is taken for each query object.
     * @param <T> the type of query objects
     * @param queryObjects the list of query objects to search for the similar ones
     * @param algorithm the algorithm used to search for similar objects
     * @param threshold the distance radius used for the {@link RangeQueryOperation}
     *          executed on the {@code algorithm} in order to retrieve the similar objects
     * @return a map of the query object and the closest object found
     * @throws AlgorithmMethodException if there was an error executing the {@link RangeQueryOperation}
     * @throws NoSuchMethodException if the algorithm does not support {@link RangeQueryOperation}
     */
    public static <T extends LocalAbstractObject> Map<T, RankedAbstractObject> findSimilarObjects(List<T> queryObjects, Algorithm algorithm, float threshold) throws AlgorithmMethodException, NoSuchMethodException {
        Map<T, RankedAbstractObject> ret = new HashMap<T, RankedAbstractObject>(queryObjects.size());
        for (T queryObject : queryObjects) {
            Iterator<? extends RankedAbstractObject> similarObjects = algorithm.getQueryAnswer(new RangeQueryOperation(queryObject, threshold, 1));
            if (similarObjects.hasNext())
                ret.put(queryObject, similarObjects.next());
        }
        return ret;
    }

    /**
     * Convenience method for processing the images of the detected faces.
     *
     * @param imageDir the directory with the original images
     * @param destDir the directory where the new face images will be stored
     * @param dataclass the class of objects to load from the data file
     * @param datafile the data file with {@link ObjectFaceLuxandDescriptor}s
     * @param locatorRegexp regular expression for matching the object locators
     * @param enlargementFactor the enlargement factor of the face bounding box;
     *          zero means no enlargement, negative value means that the original
     *          image is not cropped by the face bounding box
     * @param featureSize the size of the features dots and lines in pixels;
     *          if zero, no features are drawn
     * @throws IOException if there was a problem reading the datafile, reading
     *              an image from imageDir or writing to a file in destDir
     */
    @SuppressWarnings("UseOfSystemOutOrSystemErr")
    public static void processImages(File imageDir, File destDir, Class<? extends LocalAbstractObject> dataclass, String datafile, String locatorRegexp, double enlargementFactor, int featureSize) throws IOException {
        StreamGenericAbstractObjectIterator<LocalAbstractObject> objects = new StreamGenericAbstractObjectIterator<LocalAbstractObject>(dataclass, datafile);
        try {
            while (true) {
                FaceKey key = (FaceKey)objects.getObjectByLocatorRegexp(locatorRegexp).getObjectKey();
                BufferedImage image = key.loadFaceImage(imageDir);
                try {
                    if (featureSize > 0)
                        image = key.drawFaceLandmarks(key.drawFaceContour(image, featureSize), featureSize);
                    if (enlargementFactor >= 0) // Zero means no enlargement, negative value means no cropping
                        image = key.extractFaceImage(image, enlargementFactor);
                    else if (enlargementFactor == -2) // Large negative value means that the image is transformed for the ObjectAdvancedFaceDescriptor by eyes
                        image = key.extractTransformedFaceImageByEyes(image, new Point(31, 24), 46, 56);
                    else if (enlargementFactor <= -3) // Large negative value means that the image is transformed for the ObjectAdvancedFaceDescriptor by position
                        image = key.extractTransformedFaceImageByPos(image, new Point(16, 24), 46, 56);
                    key.saveFaceImage(image, "jpeg", destDir);
                } catch (RuntimeException e) {
                    System.err.println("Error processing " + key.getLocatorURI() + ": " + e);
                }
            }
        } catch (NoSuchElementException ignore) {
        }
        objects.close();
    }

    /**
     * Prints usage information.
     */
    @SuppressWarnings("UseOfSystemOutOrSystemErr")
    public static void usage() {
        System.err.println("Usage: " + FaceProcessor.class.getName() + " <imageDir> <destDir> <dataclass> <datafile> [<locatorRegexp> [<enlargementFactor> [<featureSize>]]]");
        System.err.println("\tSpecial value -2 of enlargementFactor means resizing (and rotation) by detected eyes,");
        System.err.println("so that the eyes are positioned at [16, 24] and [31, 24] in a 46x56 image (i.e. for the MPEG7 descriptor)");
        System.err.println("\tSpecial value -3 of enlargementFactor means resizing (and rotation) by detected face width and roll angle,");
        System.err.println("so that the eyes are positioned at [16, 24] and [31, 24] in a 46x56 image (i.e. for the MPEG7 descriptor)");
    }

    /**
     * Calls the {@link #processImages} method.
     * @param args the following arguments are expected:
     *          imageDir, destDir, dataclass, datafile,
     *          locatorRegexp, enlargementFactor, and featureSize
     * @throws IOException if there was a problem reading the datafile, reading
     *              an image from imageDir or writing to a file in destDir
     */
    @SuppressWarnings({"UseOfSystemOutOrSystemErr", "CallToThreadDumpStack"})
    public static void main(String[] args) throws IOException {
        try {
            int argIndex = 0;
            File imageDir = new File(args[argIndex++]);
            File destDir = new File(args[argIndex++]);
            Class<? extends LocalAbstractObject> dataclass = Convert.getClassForName(args[argIndex++], LocalAbstractObject.class);
            String datafile = args[argIndex++];
            String locatorRegexp = argIndex < args.length ? args[argIndex++] : ".*";
            double enlargementFactor = argIndex < args.length ? Double.parseDouble(args[argIndex++]) :  0.05;
            int featureSize = argIndex < args.length ? Integer.parseInt(args[argIndex++]) :  0;
            processImages(imageDir, destDir, dataclass, datafile, locatorRegexp, enlargementFactor, featureSize);
        } catch (IOException e) {
            System.err.println(e);
            usage();
        } catch (Exception e) {
            e.printStackTrace();
            usage();
        }
    }    
}
