
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.io.PrintStream;

/*
 *  BinarySerializableDumper
 * 
 */
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import messif.buckets.index.LocalAbstractObjectOrder;
import messif.buckets.index.impl.LongStorageMemoryIndex;
import messif.buckets.storage.impl.DiskStorage;
import messif.objects.LocalAbstractObject;
import messif.objects.util.StreamGenericAbstractObjectIterator;
import messif.utility.Convert;

/**
 *
 * @author xbatko
 */
public class LocatorStorageCreator {

    public static void usage(PrintStream out) {
        out.println("Usage: LocatorStorageCreator <class> <cacheClasses> <file> <out_storage_file> <out_index_file>");
        out.println("  class - the class of objects that are stored in the bucket");
        out.println("  cacheClasses - comma-separated list of classes that will be cached");
        out.println("  file - file with text representation of the objects");
        out.println("  out_storage_file - the disk storage file where the object are binary-serialized");
        out.println("  out_index_file - the file where the index will be serialized using ObjectOutputStream");
    }

    public static void main(String[] args) throws Throwable {
        if (args.length < 5) {
            usage(System.err);
            System.exit(1);
        }

        try {
            // Prepare parameters for the disk storage
            Class<LocalAbstractObject> objectsClass = Convert.getClassForName(args[0], LocalAbstractObject.class);  // First cmdline parameter
            Map<String, Object> params = new HashMap<String, Object>();
            params.put("readOnly", false);
            if (args[1].length() > 0) {
                params.put("cacheClasses", args[1]); // Second cmdline parameter
            }
            params.put("bufferSize", 16*1024);
            params.put("directBuffer", true);
            params.put("memoryMap", false);
            params.put("startPosition", 0L);
            params.put("file", args[3]); // Fourth cmdline parameter

            DiskStorage<LocalAbstractObject> diskStorage = DiskStorage.create(objectsClass, params);

//            ModifiableOrderedIndex<String, LocalAbstractObject> index = new LongStorageIndex<String, LocalAbstractObject>(
//                    diskStorage, LocalAbstractObjectOrder.locatorToLocalObjectComparator);
            LongStorageMemoryIndex<String, LocalAbstractObject> index = new LongStorageMemoryIndex<String, LocalAbstractObject>(
                    diskStorage, LocalAbstractObjectOrder.locatorToLocalObjectComparator);

            // Read objects from the data file
            Iterator<LocalAbstractObject> iterator = new StreamGenericAbstractObjectIterator<LocalAbstractObject>(objectsClass, args[2]);  // Third cmdline parameter
            while (iterator.hasNext())
                index.add(iterator.next());

            // Serialize index
            ObjectOutputStream indexOutput = new ObjectOutputStream(new FileOutputStream(args[4]));  // Fifth cmdline parameter
            indexOutput.writeObject(index);
            indexOutput.close();
            index.finalize();
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }
}
