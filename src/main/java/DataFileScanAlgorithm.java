

import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.Arrays;
import java.util.Collection;
import java.util.NoSuchElementException;
import java.util.concurrent.Executors;
import messif.algorithms.Algorithm;
import messif.algorithms.AlgorithmMethodException;
import messif.algorithms.AsynchronousNavigationProcessor;
import messif.algorithms.NavigationDirectory;
import messif.algorithms.NavigationProcessor;
import messif.algorithms.impl.AbstractNavigationProcessor;
import messif.objects.AbstractObject;
import messif.objects.LocalAbstractObject;
import messif.objects.util.AbstractObjectIterator;
import messif.objects.util.StreamGenericAbstractObjectIterator;
import messif.operations.AbstractOperation;
import messif.operations.QueryOperation;

/**
 * Simple indexing algorithm that implements query operations on a data file with objects.
 * Note that this algorithm is read-only.
 *
 * @author xbatko
 */
public class DataFileScanAlgorithm extends Algorithm implements NavigationDirectory {
    /** Serial version of this object used for {@link java.io.Serializable} */
    private final static long serialVersionUID = 1L;

    /** Given a space-separated string of file names, this method returns an array of strings with individual file names. */
    public static String [] getFileNames(String fileNames) {
        return fileNames.split("  *");
    }
    
    //****************** Attributes ******************//

    /** Name of the file from which the objects are loaded */
    private final Collection<String> fileNames;
    /** Class of objects stored in the file */
    private final Class<? extends LocalAbstractObject> objectClass;
    

    //****************** Constructor ******************//

    /**
     * Creates a new indexing algorithm that operates on a given file with objects.
     * @param fileNames the array of name files from which the objects are loaded
     * @param objectClass the class of objects stored in the file
     */
    @AlgorithmConstructor(description="algorithm that operates on a static file", arguments={"object class", "file names (array)"})
    public DataFileScanAlgorithm(Class<? extends LocalAbstractObject> objectClass, String... fileNames) {
        super("Data-file sequential scan");
        if (fileNames == null || fileNames.length == 0)
            throw new IllegalArgumentException("At least one file must be specified");
        this.fileNames = Arrays.asList(fileNames);
        this.objectClass = objectClass;
        if (this.fileNames.size() > 1)
            setOperationsThreadPool(Executors.newFixedThreadPool(this.fileNames.size()));
    }


    //****************** Deserialization ******************//

    /**
     * Read the serialized algorithm from an object stream.
     * @param in the object stream from which to read the disk storage
     * @throws IOException if there was an I/O error during deserialization
     * @throws ClassNotFoundException if there was an unknown object in the stream
     */
    private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
        in.defaultReadObject();
        if (fileNames.size() > 1)
            setOperationsThreadPool(Executors.newFixedThreadPool(fileNames.size()));
    }

    //****************** Query processing thread implementation ******************//

    @Override
    public <O extends AbstractOperation> NavigationProcessor<O> getNavigationProcessor(O operation) {
        if (!(operation instanceof QueryOperation)) {
            return null;
        }
        return new AbstractNavigationProcessor<O, String>(operation, false, fileNames) {
            @Override
            protected O processItem(O operation, String fileName) throws AlgorithmMethodException {
                try {
                    StreamGenericAbstractObjectIterator<LocalAbstractObject> it = new StreamGenericAbstractObjectIterator<LocalAbstractObject>(objectClass, fileName);
                    try {
                        ((QueryOperation) operation).evaluate(it);
                    } finally {
                        it.close();
                    }
                    return operation;
                } catch (IOException e) {
                    throw new AlgorithmMethodException(e);
                }
            }
        };
    }
    
                    
//            AbstractObjectIterator<LocalAbstractObject> limitedIt = new AbstractObjectIterator<LocalAbstractObject>() {
//
//                int counter = 1000;
//
//                @Override
//                public LocalAbstractObject getCurrentObject() throws NoSuchElementException {
//                    return it.getCurrentObject();
//                }
//
//                @Override
//                public boolean hasNext() {
//                    if (counter-- <= 1) {
//                        return false;
//                    }
//                    return it.hasNext();
//                }
//
//                @Override
//                public LocalAbstractObject next() {
//                    return it.next();
//                }
//
//                @Override
//                public void remove() {
//                    it.remove();
//                }
//            };    
}
