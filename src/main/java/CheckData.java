
import messif.objects.LocalAbstractObject;
import messif.objects.util.StreamGenericAbstractObjectIterator;
import messif.utility.Convert;

/**
 * This class simply reads in the data file and prints the number of the objects.
 * 
 * @author xnovak8
 */
public class CheckData {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            if (args.length < 2) {
                System.err.println("Usage: CheckData <class> <datafile>[.gz] [-print]");
                return;
            }
            
            Class<? extends LocalAbstractObject> objectClass = Convert.getClassForName(args[0], LocalAbstractObject.class);
            
            StreamGenericAbstractObjectIterator<LocalAbstractObject> iterator = new StreamGenericAbstractObjectIterator<LocalAbstractObject>(
                    objectClass, // Class of objects in file
                    args[1] // File name
                    );            
            boolean printOutput = ((args.length > 2) && ("-print".equals(args[2])));
            
            long counter = 0;
            while (iterator.hasNext()) {
                LocalAbstractObject obj = iterator.next();
                if (printOutput) {
                    obj.write(System.out);
                }
                if (++ counter % 100000 == 0) {
                    System.err.println("Processed " + counter + " objects");
                    System.err.println(obj.toString());
                }
            }
            System.err.println("Finally, processed " + counter + " objects");

        } catch (Throwable e) {
            e.printStackTrace();
        }
    }
}
