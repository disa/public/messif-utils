
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import messif.buckets.Bucket;
import messif.buckets.BucketDispatcher;
import messif.buckets.BucketStorageException;
import messif.buckets.LocalBucket;
import messif.buckets.impl.MemoryStorageBucket;
import messif.buckets.split.impl.SplitPolicyVoronoiPartitioning;
import messif.objects.LocalAbstractObject;
import messif.objects.PrecompDistPerforatedArrayFilter;
import messif.objects.util.AbstractObjectIterator;
import messif.objects.util.AbstractObjectList;
import messif.objects.util.StreamGenericAbstractObjectIterator;
import messif.pivotselection.AbstractPivotChooser;
import messif.pivotselection.KMeansPivotChooser;
import messif.statistics.Statistics;
import messif.utility.Convert;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Vlastislav Dohnal (xdohnal), dohnal@fi.muni.cz, Faculty of Informatics, Masaryk University, Brno, Czech Republic
 */
public class ClusterByPivots {

    static String fileData = "datasets/sapir.nov-demo-10m/metaobject-100000-filter-abstractkey.data";
    static String filePivots = "datasets/sapir.nov-demo-10m/queryset-50-nofilter-noskipgraphs.data";
    static String fileOutput = "datasets/sapir.nov-demo-10m/metaobject-100000-clustered.data";
    static String objectString = "messif.objects.impl.MetaObjectSAPIRWeightedDist2";
    
    static int numberOfPivots = 10;

    /** Formats the usage string */
    private static void usage() {
        System.err.println("Usage: ClusterByPivots [parameters]");
        System.err.println("Parameters:");
        System.err.println("            -h [--help]         - show this help");
        System.err.println("            -cls <object_class> - metric-object class to be used, default: " + objectString);
        
        System.err.println("            -infile <file>      - file name to read data to cluster by pivots (Voronoi partitioning). If '-', System.in is used. Default: " + fileData);
        System.err.println("            -outfile <file>     - file name to append the selected pivots to, if '-', System.out is used. Default: " + fileOutput);

        System.err.println("            -np <#_of_pivots>   - number of pivots/clusters to select, default: " + numberOfPivots);
        System.err.println("            -npuseall           - all sample file objects are used in the pivot selection; only KMeansPivotChooser is supported now.");
        System.err.println("            -pf <pivots_file>   - file with preselected pivots (first -np objects taken) Default: " + filePivots);
    }
    
    
    public static void main(String[] args) {
        Class<? extends LocalAbstractObject> objectClass = null;

        
        StringBuffer strbuf = new StringBuffer("The 'ClusterByPivots' program started with arguments: ");
        for (int i = 0; i<args.length; i++)
            strbuf.append(args[i]).append(", ");
        System.err.println(strbuf.toString());
        
        // print usage
        if ((args.length == 0) || ((args[0].equals("-h")) || (args[0].equals("--help")))) {
            usage();
            return;
        }
        
        // parse and handle the options and commands
        for (int i = 0; i < args.length; ) {
            boolean err = false;
            
            // object class
            if (args[i].equals("-cls")) {
                if (++i >= args.length)
                    err = true;
                else
                    objectClass = selectObjectClass(args[i]);
            }
            
            // number of pivots
            else if (args[i].equals("-np")) {
                if (++i >= args.length)
                    err = true;
                else
                    numberOfPivots = Integer.parseInt(args[i]);
            }
            // Use all pivots from the file
            else if (args[i].equals("-npuseall")) {
                numberOfPivots = Integer.MAX_VALUE;
            }
            // file with preselected pivots (pivot file)
            else if (args[i].equals("-pf")) {
                if (++i >= args.length)
                    err = true;
                else
                    filePivots = args[i];
            }

            // output file specification
            else if (args[i].equals("-outfile")) {
                if (++i >= args.length)
                    err = true;
                else
                    fileOutput = args[i];
            }

            // input file specification
            else if (args[i].equals("-infile")) {
                if (++i >= args.length)
                    err = true;
                else
                    fileData = args[i];
            }
            // else: unknown option
            else {
                System.err.println("Unknown option/command '"+args[i]+"'");
                System.err.println();
                usage();
                return;
            }
            
            if (err) {
                System.err.println("Missing argument to " + args[i]);
                System.err.println();
                usage();
                return;
            }
            
            i++;
        } // end of parameters parsing
        

        // if object class has not been given, use default
        if (objectClass == null) {
            objectClass = selectObjectClass(objectString);
            if (objectClass == null)
                return;
        }

        
        
        try {
            // Disable statistics
            Statistics.disableGlobally();

            BucketDispatcher disp = new BucketDispatcher(Integer.MAX_VALUE, Long.MAX_VALUE, Long.MAX_VALUE, 0, true, MemoryStorageBucket.class);

            LocalBucket bucket = disp.createBucket();
            StreamGenericAbstractObjectIterator input;
            if (fileData == null || fileData.equals("-"))
                input = new StreamGenericAbstractObjectIterator(objectClass, new BufferedReader(new InputStreamReader(System.in)));
            else
                input = new StreamGenericAbstractObjectIterator(objectClass, fileData);

            bucket.addObjects(input);
            System.out.println("Objects read from the input stream: " + bucket.getObjectCount());

            // Initial pivots...
            StreamGenericAbstractObjectIterator inputPivots = new StreamGenericAbstractObjectIterator(objectClass, filePivots);
            AbstractObjectList<LocalAbstractObject> initPivots = new AbstractObjectList<LocalAbstractObject>();
            while (inputPivots.hasNext() && numberOfPivots-- > 0)
                initPivots.add(inputPivots.next());
        
            SplitPolicyVoronoiPartitioning policy = new SplitPolicyVoronoiPartitioning();
            policy.setPivots(initPivots.toArray(new LocalAbstractObject[initPivots.size()]));

            List<Bucket> resBuckets = new ArrayList<Bucket>();
            resBuckets.add(bucket);

            System.out.println("Objects before splitting: " + bucket.getObjectCount());
            bucket.split(policy, resBuckets, disp, 0);        // This CAST is checked!!!
            long cnt = 0;
            for (Bucket b : resBuckets)
                cnt += ((LocalBucket)b).getObjectCount();
            System.out.println("Objects after splitting: " + cnt);

            BufferedOutputStream output = null;
            if (fileOutput == null || fileOutput.equals("-")) 
                output = new BufferedOutputStream(System.out);
            // Write all data out but bucket by bucket
            for (Bucket b : resBuckets) {
                if (b == null)
                    continue;
                if (fileOutput == null || fileOutput.equals("-")) {
                    System.out.println("# ##### Cluster #" + b.getBucketID());
                } else {
                    output = new BufferedOutputStream(new FileOutputStream(fileOutput + "/cluster" + b.getBucketID() + ".data"));
                }
                AbstractObjectIterator<LocalAbstractObject> iter = b.getAllObjects();
                while (iter.hasNext()) {
                    LocalAbstractObject o = iter.next();

                    PrecompDistPerforatedArrayFilter filter = o.getDistanceFilter(PrecompDistPerforatedArrayFilter.class);
                    if (filter != null)
                        o.unchainFilter(filter);
                    
                    o.write(output);
                }
                output.flush();
            }

        } catch (BucketStorageException ex) {
            Logger.getLogger(ClusterByPivots.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalArgumentException ex) {
            Logger.getLogger(ClusterByPivots.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ClusterByPivots.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    static private Class<? extends LocalAbstractObject> selectObjectClass(String className) {
        Class<? extends LocalAbstractObject> objectClass = null;
        try {
            if (!className.contains("."))
                className = "messif.objects.impl." + className;
            objectClass = Convert.getClassForName(className, LocalAbstractObject.class);
        } catch (ClassNotFoundException e) {
            System.err.println("Unknown class "+className);
            usage();
        } catch (ClassCastException e) {
            System.err.println("Class "+className+" must extend LocalAbstractObject");
            usage();
        }
        if (!LocalAbstractObject.class.isAssignableFrom(objectClass)) {
            System.err.println("Class "+className+" is not subclass of messif.objects.LocalAbstractObject");
            usage();
            objectClass = null;
        }
        return objectClass;
    }

}
