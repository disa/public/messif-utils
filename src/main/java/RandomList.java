
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import messif.objects.util.AbstractObjectList;
import messif.objects.util.AbstractObjectList;
import messif.objects.LocalAbstractObject;
import messif.objects.util.StreamGenericAbstractObjectIterator;
import messif.utility.Convert;


/**
 * Reads a list of objects from the passed file, the required number of objects is selected randomly
 * and these objects are written out to the given file name.
 * 
 * @author Vlastislav Dohnal, dohnal@fi.muni.cz, Faculty of Informatics, Masaryk University, Brno
 */
public class RandomList {

    /**
     * This class extacts the requested number of objects at random 
     * from the input file and these objects are writting to the output file.
     * 
     * @param args command line arguments - see above
     */
    public static void main(String[] args) {
        
        if (args.length != 4 && args.length != 6) {
            StringBuffer buffer = new StringBuffer("Usage: RandomList <object_class> <number_of_objects> <input_file>  [-cnt <limit_on_input objects>] <output_file>\n");
            buffer.append("Extracts the required number of objects from the input file at random.\n" + "These objects are written to the output file (the file is overwritten if it exists).\n" + "The input file is completely read into memory.\n");
            buffer.append("Params:\n");
            buffer.append("    object_class        class of the objects used in the input file\n");
            buffer.append("    number_of_objects   number of objects to select\n");
            buffer.append("    input_file          file to read objects from (stdin if '-')\n");
            buffer.append("    output_file         file to write the random objects to (stdout if '-')\n");
            buffer.append("    -cnt                set to the number of objects to read from input (optional)\n");
            System.err.println(buffer.toString());
            return;
        }

        try {
            int argIndex = 0;

            // Objects class
            final Class<? extends LocalAbstractObject> objectClass = Convert.getClassForName(args[argIndex++], LocalAbstractObject.class);
            int objCount = Integer.parseInt(args[argIndex++]);
            String inputFileName = args[argIndex++];

            // Open the input stream
            StreamGenericAbstractObjectIterator<LocalAbstractObject> iterator;
            if (inputFileName.equals("-")) {
                iterator = new StreamGenericAbstractObjectIterator<LocalAbstractObject>(objectClass, new BufferedReader(new InputStreamReader(System.in)));
            } else {
                iterator = new StreamGenericAbstractObjectIterator<LocalAbstractObject>(objectClass, inputFileName);
            }
            
            int limit = -1;
            if ("-cnt".equals(args[argIndex])) {
                argIndex++;
                limit = Integer.parseInt(args[argIndex++]);
            }

            // Read all objects
            AbstractObjectList<LocalAbstractObject> list = new AbstractObjectList<LocalAbstractObject>(iterator, limit);
            
            // Get the random list
            AbstractObjectList<LocalAbstractObject> randomList = new AbstractObjectList<LocalAbstractObject>();
            list.randomList(objCount, true, randomList);

            String outputFileName = args[argIndex++];
            // Open the output stream
            OutputStream outStream;
            if (outputFileName.equals("-")) {
                outStream = System.out;
            } else {
                outStream = new BufferedOutputStream(new FileOutputStream(outputFileName, false));
            }

            int i = 1;
            for (LocalAbstractObject o : randomList) {
                //System.err.println("Obj[" + i + "]: " + o);
                o.write(outStream);
                i++;
            }
            
            // Write all objects out
            outStream.flush();
            
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }
}
