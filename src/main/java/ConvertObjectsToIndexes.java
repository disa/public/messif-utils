
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import messif.objects.LocalAbstractObject;
import messif.objects.util.AbstractObjectIterator;
import messif.objects.util.AbstractObjectList;
import messif.objects.util.StreamGenericAbstractObjectIterator;
import messif.utility.Convert;
import messif.utility.SortedCollection;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Utility for converting objects to relative indexes with respect to the object's ordering in a given file.
 * The inverse conversion is supported too.
 *
 * @author Vlastislav Dohnal (xdohnal), dohnal@fi.muni.cz, Faculty of Informatics, Masaryk University, Brno, Czech Republic
 */
public class ConvertObjectsToIndexes {

    protected static final int DIRECTION_UNDEFINED = -1;
    protected static final int DIRECTION_TO_INDEXES = 0;
    protected static final int DIRECTION_TO_OBJECTS = 1;

    protected static void printUsage() {
        // Wrong argument count
        System.err.println("Usage: ConvertObjectsToIndexes -cls <objects' class> {{-toindexes|-toobjects} <data_file>} [-out <output object file>|-ext <extension>] [-addradius <value|interval>] [-overwrite] [<input file1> ...]");
        System.err.println("       Converts all files passed on the command-line to indexes or back from indexes to objects based on the <data_file>.");
        System.err.println("       If -toindexes is specified, the input file contains objects that are converted to relative indexes based on the positions of the input objects in <data_file>");
        System.err.println("       If -toobjects is specified, the input file contains indexes that are converted to objects at the indexes in <data_file>");
        System.err.println();
        System.err.println("       Input file names are read as parameters on the command-line after parsing all options passed.");
        System.err.println("       If none input file is specified, System.in is used instead.");
        System.err.println("       Output is written to an output file specified the -out parameter or if -ext is used, the passed extension is appended to the input file name and this file is used to append output to.");
        System.err.println("       If the output file does not exist, it is created. Use -overwrite option to overwrite any output file.");
        System.err.println("       If the output file is not specified or -ext is used when no input file name is passed, System.out is used.");
        System.err.println();
        System.err.println("       The format of index file is simple: each line is of format:  <index> <radius>");
        System.err.println("       The field radius is optional.");
        System.err.println();
        System.err.println("       The option -addradius is allowed only if -toindexes is specified.");
        System.err.println("       If option -addradius is used, a value of radius is appended to each output line (space separated).");
        System.err.println("       The parameter to addradius is three fold: ");
        System.err.println("           1/ fixed value");
        System.err.println("           2/ random number within an interval specified by number:number");
        System.err.println("           3/ interval (number:number) -- radii are read from as input file, the file name is parsed from the <input file> parameter which is then in the form of: <input file>:<radius file>");
        System.err.println("              The values read from the radius file are checked against this interval and it must fall within in. If not, the lower/upper bound of the interval is used.");
    }

    public static void main(String... args) throws Exception {
        String outputFile = null;
        String outputExt = null;
        String dataFile = null;
        int direction = DIRECTION_UNDEFINED;
        boolean append = true;

        float radMin = -1f;
        float radMax = -1f;

        // print usage
        if (args.length == 0 || args[0].equals("-h") || args[0].equals("--help")) {
            printUsage();
            return;
        }

        // Objects class
        Class<LocalAbstractObject> objectsClass = null;

        // parse and handle the options and commands
        int argidx = 0;
        for (; argidx < args.length; argidx++) {
            boolean err = false;

            if (! args[argidx].startsWith("-")) {
                // End of options
                break;
            }

            if (args[argidx].equals("-cls")) {
                if (++argidx >= args.length)
                    err = true;
                else
                    objectsClass = Convert.getClassForName(args[argidx], LocalAbstractObject.class);
            } else
            if (args[argidx].equals("-out")) {
                if (++argidx >= args.length)
                    err = true;
                else
                    outputFile = args[argidx];
            } else
            if (args[argidx].equals("-ext")) {
                if (++argidx >= args.length)
                    err = true;
                else {
                    outputExt = args[argidx];
                    if (!outputExt.startsWith("."))
                        outputExt = "." + outputExt;
                }
            } else

            if (args[argidx].equals("-toindexes") || args[argidx].equals("-toobjects")) {
                if (direction != DIRECTION_UNDEFINED)
                    err = true;
                else
                    direction = (args[argidx].equals("-toindexes")) ? DIRECTION_TO_INDEXES : DIRECTION_TO_OBJECTS;
                
                if (++argidx >= args.length)
                    err = true;
                else
                    dataFile = args[argidx];
            } else

            if (args[argidx].equals("-addradius")) {
                if (++argidx >= args.length)
                    err = true;
                else {
                    String[] interval = args[argidx].split(":");
                    if (interval.length == 0)
                        err = true;
                    else {
                        radMin = Float.parseFloat(interval[0]);
                        if (interval.length > 1)
                            radMax = Float.parseFloat(interval[1]);
                        else
                            radMax = radMin;
                        if (radMin < 0f || radMax < 0f)
                            throw new NumberFormatException("Radius cannot be negative!");
                    }
                }
            } else

            if (args[argidx].equals("-overwrite")) {
                append = false;
            } else

            {
                // Unknown option...
                System.err.println("Unknown option " + args[argidx]);
                System.err.println();
                printUsage();
                return;
            }

            if (err) {
                System.err.println("Missing or bad argument to " + args[argidx] + " or unknown parameter.");
                System.err.println();
                printUsage();
                return;
            }
        }

        // Mandatory parameter
        if (objectsClass == null) {
                System.err.println("Argument -cls <class_name> has not been passed.");
                System.err.println();
                printUsage();
                return;
        }

        AbstractObjectIterator<LocalAbstractObject> data = null;
        AbstractObjectList<LocalAbstractObject> dataCol = null;

        // Test how many input file are passed
        int inputLen = args.length - argidx;
        data = new StreamGenericAbstractObjectIterator<LocalAbstractObject>(objectsClass, dataFile);
        if (inputLen > 1)
            dataCol = new AbstractObjectList<LocalAbstractObject>(data);
        else if (inputLen == 0)
            inputLen++;         // Read System.in

        // Process all input files...
        for (int i = 0; i < inputLen; i++) {
            BufferedReader input = null;
            BufferedReader radius = null;
            String inputFile = null;
            String radiusFile = null;
            try {
                // Check for ':' in the file name...
                String[] names = args[argidx+i].split(":");
                if (names.length == 2 && radMin >= 0) {
                    inputFile = names[0];
                    radiusFile = names[1];
                    radius = new BufferedReader( new InputStreamReader(new FileInputStream(radiusFile)) );
                } else
                    inputFile = args[argidx+i];

                //System.err.println("Processing input file: " + inputFile);

                input = new BufferedReader( new InputStreamReader(new FileInputStream(inputFile)) );
            } catch (ArrayIndexOutOfBoundsException ex) {
                // Read System.in...
                input = new BufferedReader( new InputStreamReader(System.in) );
            }
            OutputStream output;
            if (outputFile != null)
                output = new FileOutputStream(outputFile, append);
            else if (outputExt != null && inputFile != null) {
                output = new FileOutputStream(inputFile + outputExt, append);
            } else
                output = System.out;

            // Set data collection iterator
            if (dataCol != null)
                data = dataCol.iterator();
            
            // Start processing
            switch (direction) {
                case DIRECTION_TO_INDEXES:
                    toIndexes(objectsClass, data, input, output, radMin, radMax, radius);
                    break;
                case DIRECTION_TO_OBJECTS:
                    toObjects(data, input, output, radMin, radMax);
                    break;
                default:
                    System.err.println("Argument -toindexes or -toobjects is missing!!!");
                    System.err.println();
                    printUsage();
                    return;
            }

            input.close();
            if (output != System.out)
                output.close();
        }

        System.err.println("All done");
    }

    private static void toIndexes(Class<LocalAbstractObject> objectsClass, AbstractObjectIterator<LocalAbstractObject> data,
                                  BufferedReader input, OutputStream output, float radMin, float radMax, BufferedReader radii) throws IOException {
        float radDiff = radMax - radMin;

        // Read all input data
        Map<Integer,ObjectPositionPair> objects = new HashMap<Integer,ObjectPositionPair>();
        AbstractObjectIterator<LocalAbstractObject> inputIter = new StreamGenericAbstractObjectIterator<LocalAbstractObject>(objectsClass, input);
        int i = 0;
        while (inputIter.hasNext()) {
            LocalAbstractObject obj = inputIter.next();
            ObjectPositionPair newPair = new ObjectPositionPair(obj,i);
            ObjectPositionPair prevPair = objects.put(newPair.getKey(), newPair);
            if (prevPair != null)
                newPair.setNextPair(prevPair);
            i++;
        }

        // Sorted collection for outputing objects' indexes in the same order as the input objects.
        SortedCollection<IndexPositionPair> outCol = new SortedCollection<IndexPositionPair>(IndexPositionPair.getComparatorByPosition());

        // Read all data object one by one and compare with the input data
        i = 0;
        while (data.hasNext()) {
            LocalAbstractObject dataObject = data.next();

            ObjectPositionPair prevInputPair = null;
            
            for (ObjectPositionPair inputPair = objects.get(dataObject.dataHashCode()); (inputPair != null); inputPair = inputPair.getNextPair()) {
                if (inputPair.getObject().dataEquals(dataObject)) {
                    IndexPositionPair ipp = new IndexPositionPair(i, inputPair.getPositionInInput());
                    ipp.setObject(dataObject);
                    outCol.add(ipp);

                    // Remove this pair because it has been sent to output
                    if (prevInputPair == null) {
                        // Removing the first pair in the chain
                        ObjectPositionPair next = inputPair.getNextPair();
                        if (next == null)
                            objects.remove(inputPair.object.dataHashCode());
                        else
                            objects.put(next.getKey(), next);
                    } else {
                        // Removing any other in the chain
                        prevInputPair.setNextPair(inputPair.getNextPair());
                    }
                }
                prevInputPair = inputPair;
            }
            i++;
        }

        for (IndexPositionPair p : outCol) {
            String line;
            if (radMin != -1f) {
                float rad = radMin;
                if (radDiff > 0f) {
                    if (radii == null)         // Generate radius randomly
                        rad += ((float)Math.random()) * radDiff;
                    else {                      // Read radius from the file
                        rad = Float.parseFloat(radii.readLine());
                        if (rad < radMin)
                            rad = radMin;
                        else if (rad > radMax)
                            rad = radMax;
                    }
                }
                line = String.format(Locale.ENGLISH, "%d %f\n", p.indexInFile, rad);
            } else {
                line = String.format("%d\n", p.indexInFile);
            }
            output.write(line.getBytes());
        }

        // If not all input objects were converted, print warning
        if (objects.size() > 0)
            System.err.println("Not all input objects got converted to indexes!!!");
    }

    private static class ObjectPositionPair {
        protected final LocalAbstractObject object;
        protected final int positionInInput;

        protected ObjectPositionPair nextPair = null;

        public ObjectPositionPair(LocalAbstractObject object, int positionInInput) {
            this.object = object;
            this.positionInInput = positionInInput;
        }

        public void setNextPair(ObjectPositionPair nextPair) {
            this.nextPair = nextPair;
        }

        public ObjectPositionPair getNextPair() {
            return nextPair;
        }

        public LocalAbstractObject getObject() {
            return object;
        }

        public int getPositionInInput() {
            return positionInInput;
        }

        public int getKey() {
            return object.dataHashCode();
        }
    }

    private static class IndexPositionPair {
        public final int indexInFile;
        public final int positionInInput;

        /** Assocated data object */
        LocalAbstractObject object;

        public IndexPositionPair(int indexInFile, int positionInInput) {
            this.indexInFile = indexInFile;
            this.positionInInput = positionInInput;
        }

        public LocalAbstractObject getObject() {
            return object;
        }

        public void setObject(LocalAbstractObject object) {
            this.object = object;
        }


        public static Comparator<IndexPositionPair> getComparatorByIndex() {
            return new Comparator<IndexPositionPair>() {
                public int compare(IndexPositionPair o1, IndexPositionPair o2) {
                    // First, compare indexes
                    if (o1.indexInFile < o2.indexInFile)
                        return -1;
                    else if (o1.indexInFile > o2.indexInFile)
                        return 1;
                    // Second, compare positions
                    if (o1.positionInInput < o2.positionInInput)
                        return -1;
                    else if (o1.positionInInput > o2.positionInInput)
                        return 1;
                    // Same
                    return 0;
                }
            };
        }

        public static Comparator<IndexPositionPair> getComparatorByPosition() {
            return new Comparator<IndexPositionPair>() {
                public int compare(IndexPositionPair o1, IndexPositionPair o2) {
                    // First, compare positions
                    if (o1.positionInInput < o2.positionInInput)
                        return -1;
                    else if (o1.positionInInput > o2.positionInInput)
                        return 1;
                    // Second, compare indexes
                    if (o1.indexInFile < o2.indexInFile)
                        return -1;
                    else if (o1.indexInFile > o2.indexInFile)
                        return 1;
                    // Same
                    return 0;
                }
            };
        }
    }

    private static void toObjects(AbstractObjectIterator<LocalAbstractObject> data, BufferedReader input, OutputStream output, float radMin, float radMax) throws IOException {
        String line;
        SortedCollection<IndexPositionPair> indexes = new SortedCollection<IndexPositionPair>(IndexPositionPair.getComparatorByIndex());

        // Read the input lines of format "index radius" where radius is optional.
        int i = 0;
        while ((line = input.readLine()) != null) {
            String[] parts = line.split("[ ,]");
            if (parts.length == 0)
                throw new IllegalArgumentException("Input line format error! The input file must contain lines: 'number number' or just 'number'.");
            indexes.add( new IndexPositionPair(Integer.parseInt(line), i++) );
        }

        // Read the data objects
        int delta = 0;
        for (IndexPositionPair p : indexes) {
            p.setObject(data.getObjectByPosition(p.indexInFile - delta));
            delta = p.indexInFile;
        }

        // Re-sort the objects read by the order in the input file
        SortedCollection<IndexPositionPair> positions = new SortedCollection<ConvertObjectsToIndexes.IndexPositionPair>(IndexPositionPair.getComparatorByPosition());
        positions.addAll(indexes);
        for (IndexPositionPair p : positions)
            p.getObject().write(output);
    }
}
