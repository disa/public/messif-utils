import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Stack;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
/*
 * SapirXmlToDB.java
 *
 * Created on 21. zari 2007, 9:28
 *
 */

/**
 *
 * @author xbatko
 */
public class SapirXmlToDB extends DefaultHandler {
    
    protected static final String dbconn = "jdbc:mysql://andromeda/search";
    protected static final String dbuser = "search";
    protected static final String dbpass = "hledani";
    
    protected final Connection connection;
    protected final PreparedStatement sapirInsert;
    protected final PreparedStatement sapirTagsInsert;
    
    public SapirXmlToDB() throws SQLException {
        // Open connection to database
        connection = DriverManager.getConnection(dbconn, dbuser, dbpass);
        
        // Prepare insert statements
        sapirInsert = connection.prepareStatement("insert into sapir(" +
                "id, dateuploaded, username, realname, location, title, url, description, comments, notes" +
                ") values (" +
                "?, ?, ?, ?, ?, ?, ?, ?, ?, ?" +
                ")");
        sapirTagsInsert = connection.prepareStatement("insert into sapir_tags(sapir_id, name) values (?, ?)");
    }
    
    /**
     * Recursively search directories for XML files. For every file a meta object is created
     * and its descriptors written to the respective streams.
     * 
     * @param dir the directory to start the search from
     * @param metaobjectStream the stream to store whole meta object (stored only if status code matches <code>successMask</code>)
     * @param descriptorNames the list of descriptor names to extract from the meta object
     * @param descriptorStreams the list of streams where the descriptors sould be written; it must have the same length as <code>descriptorNames</code>
     * @param descriptorLocatorSeparators the list of locator separators for the respective descriptors; it must have the same length as <code>descriptorNames</code>
     * @param successMask the status code that is considered successful
     * @return number of loaded meta objects
     */
    public int searchDir(File dir, SAXParser parser) {
        int rtv = 0;
        for (File file : dir.listFiles())
            try {
                if (file.isDirectory()) {
                    rtv += searchDir(file, parser);
                } else if (file.getName().toLowerCase().endsWith(".xml")) {
                    parser.parse(file, this);
                    rtv++;
                }
            } catch (Exception e) {
                System.err.print("Error processing '");
                System.err.print(file);
                System.err.print("': ");
                System.err.println(e);
            }

        return rtv;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws SQLException, ParserConfigurationException, SAXException {
        SapirXmlToDB handler = new SapirXmlToDB();
        SAXParser parser = SAXParserFactory.newInstance().newSAXParser();

        if (args.length > 0)
            for (String arg : args)
                handler.searchDir(new File(arg), parser);
        else handler.searchDir(new File("."), parser);
    }
    
    /****************** Parsing methods ******************/
    
    protected final Stack<String> elementNamesStack = new Stack<String>();
    protected final StringBuffer textBuffer = new StringBuffer();
    protected boolean parsing = false;
    protected int sapirId = 0;
    
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        String name = (localName.length() == 0)?qName:localName;
        elementNamesStack.push(name);
        
        // Opening photo tag
        if (name.equals("photo") && elementNamesStack.size() == 2) {
            try {
                sapirId = Integer.parseInt(attributes.getValue("id"));
                sapirInsert.setInt(1, sapirId);
                String attr;
                attr = attributes.getValue("dateuploaded"); sapirInsert.setInt(2, (attr == null)?null:Integer.parseInt(attr));
                attr = attributes.getValue("username"); sapirInsert.setString(3, attr);
                attr = attributes.getValue("realname"); sapirInsert.setString(4, attr);
                attr = attributes.getValue("location"); sapirInsert.setString(5, attr);
            } catch (SQLException e) {
                sapirId = 0;
                return;
            } catch (NumberFormatException e) {
                sapirId = 0;
                return;
            }
        }
        
        if (sapirId > 0 && name.equals("title") || name.equals("url") || name.equals("description") || name.equals("comments") || name.equals("notes") || name.equals("tag")) {
            parsing = true;
            textBuffer.setLength(0);
        }
    }
    
    public void endElement(String uri, String localName, String qName) throws SAXException {
        String name = elementNamesStack.pop();
        if (localName.length() == 0) {
            if (!qName.equals(name))
                throw new SAXException("Closing element '" + qName + "', but should close '" + name + "'");
        } else {
            if (!localName.equals(name))
                throw new SAXException("Closing element '" + localName + "', but should close '" + name + "'");
        }
        
        try {
            if (sapirId > 0 && name.equals("photo")) {
                sapirInsert.execute();
                sapirId = 0;
            } else if (parsing) {
                if (name.equals("title")) {
                    sapirInsert.setString(6, textBuffer.toString());
                    parsing = false;
                } else if (name.equals("url")) {
                    sapirInsert.setString(7, textBuffer.toString());
                    parsing = false;
                } else if (name.equals("description")) {
                    sapirInsert.setString(8, textBuffer.toString());
                    parsing = false;
                } else if (name.equals("comments")) {
                    sapirInsert.setString(9, textBuffer.toString());
                    parsing = false;
                } else if (name.equals("notes")) {
                    sapirInsert.setString(10, textBuffer.toString());
                    parsing = false;
                } else if (name.equals("photo")) {
                    sapirInsert.execute();
                    parsing = false;
                } else if (name.equals("tag")) {
                    sapirTagsInsert.setInt(1, sapirId);
                    sapirTagsInsert.setString(2, textBuffer.toString());
                    sapirTagsInsert.execute();
                    parsing = false;
                }
            }
        } catch (SQLException e) {
            throw new SAXException(e);
        }
    }
    
    public void characters(char[] ch, int start, int length) throws SAXException {
        if (parsing)
            textBuffer.append(ch, start, length);
    }
    
}
