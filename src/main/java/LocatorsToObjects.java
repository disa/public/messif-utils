/*
 * IncreaseDataset.java
 *
 * Created on May 9, 2007
 */

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;
import messif.objects.LocalAbstractObject;
import messif.objects.util.AbstractObjectIterator;
import messif.objects.util.StreamGenericAbstractObjectIterator;
import messif.utility.Convert;

/**
 * This class reads in file containing locators and file with a abstract objects and prints the filtered list of objects.
 *
 * @author Michal Batko, Masaryk University, Brno, Czech Republic, batko@fi.muni.cz
 * @author David Novak, FI Masaryk University, Brno, Czech Republic; <a href="mailto:xnovak8@fi.muni.cz">xnovak8@fi.muni.cz</a>
 */
public class LocatorsToObjects {

    /**
     * Returns a set of strings read from the given file.
     * @param fileName the file the lines of which are read
     * @return a set of strings read from the given file
     * @throws IOException if there was an error reading the file
     */
    public static Set<String> fileToLocators(String fileName) throws IOException {
        Set<String> locators = new LinkedHashSet<String>();
        BufferedReader reader = new BufferedReader(new FileReader(fileName));
        try {
            String line = reader.readLine();
            while (line != null) {
                locators.add(line);
                line = reader.readLine();
            }
        } finally {
            reader.close();
        }
        return locators;
    }

    /**
     * Returns an array of objects from a given iterator that are in a given set.
     * @param iterator the iterator of objects
     * @param locators the set of locators to get from the iterator
     * @return an array of objects
     */
    public static LocalAbstractObject[] objectsByLocators(AbstractObjectIterator<? extends LocalAbstractObject> iterator, Set<String> locators) {
        List<LocalAbstractObject> ret = new ArrayList<LocalAbstractObject>(locators.size());
        for (;;) {
            try {
                ret.add(iterator.getObjectByAnyLocator(locators, false));
            } catch (NoSuchElementException e) {
                break;
            }
        }
        return ret.toArray(new LocalAbstractObject[ret.size()]);
    }

    /**
     * Returns an array of objects from a given iterator that are in a given set.
     * @param iterator the iterator of objects
     * @param fileName the file from which to read the set of locators to get from the iterator
     * @return an array of objects
     * @throws IOException if there was an error reading the locators file
     */
    public static LocalAbstractObject[] objectsByLocatorsInFile(AbstractObjectIterator<? extends LocalAbstractObject> iterator, String fileName) throws IOException {
        return objectsByLocators(iterator, fileToLocators(fileName));
    }

    /**
     * This class reads in file containing locators and file with a abstract objects and prints the list 
     * of objects in the same order as in the locators file.
     * <pre>
     *  Usage: LocatorsToObjects class locators_file data_file
            Read in file containing locators and file with objects of given class and print the list
            of objects in the same order as in the locators file
            Params:
                class            the class of the data objects; extends LocalAbstractObject
                locators_file    file with locators to get from the dataset
                data_file        file with the dataset
     * </pre>
     * @param args command line arguments - see above
     */
    public static void main(String[] args) throws Exception {
        if (args.length < 3) {
            System.err.println("Usage: LocatorsToObjects <class> <locators_file> <data_file>");
            System.err.println("Read in file containing locators and file with objects of given class and print the list");
            System.err.println("of objects in the same order as in the locators file.");
            System.err.println("Params:");
            System.err.println("\t class            the class of the data objects; extends LocalAbstractObject");
            System.err.println("\t locators_file    file with the dataset");
            System.err.println("\t data_file        file with the dataset");
            return;
        }

        /************   Parsing arguments ****************/
        // The class of the dataset - extends LocalAbstractObject
        Class<? extends LocalAbstractObject> objectClass = Convert.getClassForName(args[0], LocalAbstractObject.class);
        // The locators
        Set<String> locators = fileToLocators(args[1]);
        // The data file
        StreamGenericAbstractObjectIterator<LocalAbstractObject> iterator = 
                new StreamGenericAbstractObjectIterator<LocalAbstractObject>(objectClass, args[2]);

        for (;;) {
            try {
                iterator.getObjectByAnyLocator(locators, false).write(System.out);
            } catch (NoSuchElementException e) {
                break;
            }
        }
    }
}
