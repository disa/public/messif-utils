import com.ice.tar.FastTarStream;
import com.ice.tar.TarEntry;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Utility for creating index into TAR files.
 * Please note that the database connection must be updated as well as the fileNameToLocator method.
 * @author Michal Batko <batko@fi.muni.cz>
 */
public class TarIndexer {
    private static long fileNameToLocator(String fileName) throws NumberFormatException {
        // Strip path
        int pos = fileName.lastIndexOf('/');
        int dotpos = fileName.lastIndexOf('.');
        if (dotpos == -1)
            dotpos = fileName.length();
        return Long.parseLong(fileName.substring(pos + 1, dotpos));
    }

    private static void processFile(String file, PreparedStatement insertStm) throws IOException, SQLException {
        long start = System.currentTimeMillis();
        FastTarStream tar = new FastTarStream(new FileInputStream(file));
        file = file.substring(file.lastIndexOf('/') + 1);
        try {
            for (TarEntry entry = tar.getNextEntry(); entry != null; entry = tar.getNextEntry()) {
                long locator = fileNameToLocator(entry.getName());
                insertStm.setLong(1, locator);
                insertStm.setString(2, file);
                insertStm.setLong(3, tar.getStreamPosition());
                insertStm.setLong(4, entry.getSize());
                insertStm.addBatch();
                insertStm.clearParameters();
            }
        } finally {
            tar.close();
        }
        long dbStart = System.currentTimeMillis();
        insertStm.executeBatch();
        Logger.getLogger(TarIndexer.class.getName()).log(Level.INFO, "File {0} processed in {1}ms (DB time: {2}ms)", new Object[]{
            file, System.currentTimeMillis() - start, System.currentTimeMillis() - dbStart
        });
    }

    public static void main(String[] args) throws Exception {
        String jdbcUrl = "jdbc:mysql://andromeda.fi.muni.cz/datasets?rewriteBatchedStatements=true";
        String jdbcUser = "datasets";
        String jdbcPassword = "datasets";
        String tableName = "images_new_export_2015";
        
        Connection dbConn = DriverManager.getConnection(jdbcUrl, jdbcUser, jdbcPassword);
        PreparedStatement insertStm = dbConn.prepareStatement("INSERT INTO " + tableName + "(locator, filename, position, size) values (?, ?, ?, ?)");
        //processFile("S:/datasets/profimedia/images-new-export-2015/image-tars/0000000003-0000012145.tar", insertStm);
        for (String arg : args) {
            processFile(arg, insertStm);
        }
        dbConn.close();
    }
}
