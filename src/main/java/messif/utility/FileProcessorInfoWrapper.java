/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package messif.utility;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.MessageFormat;

/**
 * Wrapper for any {@link FileProcessor} that provides information about the
 * processing state.
 *
 * @author Michal Batko, Masaryk University, Brno, Czech Republic, batko@fi.muni.cz
 */
public class FileProcessorInfoWrapper implements FileProcessor {
    /** Wrapped processor that does the actual processing */
    private final FileProcessor processor;
    /** Output where the processing info is written */
    private final OutputStream out;
    /** Message to output, use {0} placeholder for the number of objects processed and {1} for time elapsed (in milliseconds) */
    private final MessageFormat message;
    /** Number of items processed between notifications */
    private final int notifyCount;
    /** Next notification total count */
    private int nextNotify;
    /** Number of items processed */
    private int totalCount;
    /** Time this processor was first called */
    private final long startTime;

    /**
     * Creates a new FileProcessor wrapper.
     * @param processor the wrapped processor that does the actual processing
     * @param out the output where the processing info is written
     * @param message the message format string to output; use {0} placeholder for
     *          the number of objects processed and {1} for time elapsed (in milliseconds)
     * @param notifyCount the number of items processed between notifications
     */
    public FileProcessorInfoWrapper(FileProcessor processor, OutputStream out, String message, int notifyCount) {
        this.processor = processor;
        this.out = out;
        this.message = new MessageFormat(message);
        this.notifyCount = notifyCount;
        this.nextNotify = notifyCount;
        this.startTime = System.currentTimeMillis();
    }

    @Override
    public int processFile(String source, InputStream fileData) throws FileProcessorException {
        int count = processor.processFile(source, fileData);
        totalCount += count;
        if (totalCount >= nextNotify) {
            nextNotify += totalCount + notifyCount;
            onNotify();
        }
        return count;
    }

    /**
     * Returns the number of items processed so far by this processor.
     * @return the number of items processed so far by this processor
     */
    public int getCount() {
        return totalCount;
    }

    /**
     * Returns the time that elapsed since this processor has been created in milliseconds.
     * @return the elapsed time in milliseconds
     */
    public long getTimeElapsed() {
        return System.currentTimeMillis() - startTime;
    }

    /**
     * Notification method that outputs the formatted {@link #message} to the {@link #out output}.
     * @throws FileProcessorException if there was a problem writing the message to output
     */
    protected void onNotify() throws FileProcessorException {
        try {
            out.write(message.format(new Object[] { getCount(), getTimeElapsed() }).getBytes());
        } catch (IOException e) {
            throw new FileProcessorException("Cannot write info message to output: " + e, e);
        }
    }
}
